const Analysis = require('./Analysis')
const Socket = require('./Socket')

class Client {

  constructor () {
    this._waitInterval = 10
    this._step = 0.1
    this._socket = new Socket()
    this._socket.analysis = new Analysis()
    this._direction = 1
  }

  get name () {
    return this._socket.name
  }

  set name (value) {
    this._socket.name = value
  }

  set speed (value) {
    if (value <= 0 ) { // very slow!
      this._waitInterval = 1000
    } else if (value > 10) {
      this._waitInterval = 10
    } else {
      this._waitInterval = 100/value
    }
  }

  set step (value) {
    if (value <= 0) return
    this._step = value
  }

  async connect () {
    await this._socket.connect()
    this._socket.askForCurrentAnalysis()
    // wait for analysis...
    return new Promise ((resolve) => {
      setTimeout(() => {
        resolve()
      }, 100);
    })
  }

  disconnect () {
    this._socket.disconnect()
  }

  create (label) {
    this._socket.analysis.add(label)
    this._emit(label.currentOp())
  }

  change (label) {
    this._labelToChange = label
    return this
  }

  move (label) {
    this._direction = 1
    return this.change(label)
  }

  toTheLeft () {
    this._direction = -1
    return this
  }

  async of (x) {
    let times = x / this._step
    await this._multiMovesLabel(times, this._step)
  }

  async toLine (line) {
    this._labelToChange.line = line
    await this._emit(this._labelToChange.currentOp())
  }

  async toType (type) {
    this._labelToChange.type = type
    await this._emit(this._labelToChange.currentOp())
  }

  async toTag (tag) {
    this._labelToChange.tag = tag
    await this._emit(this._labelToChange.currentOp())
  }

  async toComment (comment) {
    this._labelToChange.comment = comment
    await this._emit(this._labelToChange.currentOp())
  }

  async toAttribute (name, value) {
    this._labelToChange.setAttribute(name, value)
    await this._emit(this._labelToChange.currentOp())
  }

  async _emit (operation) {
    this._socket.emitOperation(operation)
    return new Promise ((resolve) => {
      setTimeout(() => resolve(), this._waitInterval*(Math.random()+1));
    })
  }
  async _moves (step, opId, changeId) {
    this._labelToChange.move(step, this._direction)
    let op = this._labelToChange.currentOp()
    await this._emit(op)
  }
  async _multiMovesLabel (times, step) {
    for (let i = 0; i < times; i++) {
      await this._moves(step)
    }
  }

  get emitted () {
    return this._socket._emitted
  }

  get accepted () {
    return this._socket._accepted
  }

  get denied () {
    return this._socket._denied
  }

  reinit () {
    this._socket.reinit()
  }

  async changeAnalysis (labels) {
    await this._emit({
      id : 4465,
      action: "LOAD_NEW_ANALYSIS",
      labels : labels
    })
  }

}

module.exports = Client;
