const Client = require('./Client')
const Label = require('./Label')

async function delay (s) {
  return new Promise(resolve => {
    setTimeout(function () {
      resolve()
    }, s);
  })
}

module.exports.delay = delay

module.exports.test = async function test (nbClients, action) {
  let clients = []
  for (let i = 0; i < nbClients; i++) {
    let client = new Client()
    client.name = 'client-' + i.toString()
    client.step = 1
    await client.connect()
    clients.push(client)
  }
  return Promise.all(clients.map(action))
}

module.exports.init = async function init () {
  let client = new Client()
  await client.connect()
  client.reinit()
  let label = new Label()
  client.create(label)
  await delay(1000)
  client.disconnect()
  return label
}

module.exports.Stats = class Stats {

  constructor (nbClients) {
    this.emitted = 0
    this.denied = 0
    this.accepted = 0
    this._lastDate = Date.now()
    this.lastClientTime = 0
    this._nbClients = nbClients
  }

  report () {
    let acceptedRate = Math.round(100*100*this.accepted/this.emitted)/100
    let deniedRate = Math.round(100*100*this.denied/this.emitted)/100
    let missedAcks = this.emitted - this.accepted - this.denied
    let now = Date.now()
    console.log(
      ((now - this._lastDate)/1000) + " " + this.lastClientTime/this._nbClients +
      " [nb clients == " + this._nbClients + "] " +
      "EMITTED : " + this.emitted +
      " - ACCEPTED : " + this.accepted + " (" + acceptedRate + "%)" +
      " - DENIED : " + this.denied + " (" + deniedRate + "%)" +
      " - MISSED ACKS : " + missedAcks
    );
    this._lastDate = now
  }

}
