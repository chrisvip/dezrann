const Label = require('./lib/Label')
const Client = require('./lib/Client')
const Utils = require('./lib/Utils')

async function action (client, label, stats, nbKeys) {
  let lastClientDate = Date.now()
  while (client.accepted < 100) {
    let key = "key" + Math.round(Math.random()*nbKeys).toString()
    await client.change(label).toAttribute(key, "value of " + key + " for " + client.name)
  }
  stats.lastClientTime += (Date.now() - lastClientDate)/1000
  stats.emitted += client.emitted
  stats.accepted += client.accepted
  stats.denied += client.denied
  client.disconnect()
}

async function main () {
  const args = process.argv.slice(2)
  let nbClients = args[0]
  let conflict = (args[1] != 0)
  let nbKeys = args[2]
  let label
  let stats = new Utils.Stats(nbClients)
  if (conflict) {
    label = await Utils.init()
  }
  await Utils.test(nbClients, async client => {
    if (!conflict) {
      label = new Label()
      client.create(label)
    }
    await action(client, label, stats, nbKeys)
  })
  stats.report()
}

main()
