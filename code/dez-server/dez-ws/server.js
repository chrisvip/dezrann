let express = require('express');
let app = express();
let corpus = require('./corpus');
let bodyParser = require('body-parser')
let jwt = require('jsonwebtoken')
let fs = require('fs');

const PORT = 8888

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

respond = function (response, data, err, raw) {
    if (err) {
        console.log ("! " + err)
        if (err.code === 'ENOENT') {
            response.writeHead(404, "FILE NOT FOUND! -> " + err.message);
        } else if (err == "FORBIDDEN") {
          response.writeHead(401, "access forbidden!")
        } else {
            response.writeHead(500, err.message);
        }
        response.end();
    } else {
        response.writeHead(
            200,
            {
                'Content-Type': 'text/json; charset=utf-8',
                'Access-Control-Allow-Origin': '*'
            }
        );
        response.end((typeof raw === 'undefined' || !raw) ? JSON.stringify(data, null, 2) : data);
    }
}

app.use((req, res, next) => {
  if (req.headers.authorization) {
    let token = req.headers.authorization.split('Bearer ')[1]
    try {
      let data = jwt.verify(token, fs.readFileSync('public.key', 'utf8'), { algorithm: ["RS256"] })
      req.user = {
        uid : data.sub,
        groups : data.groups
      }
    } catch (e) {
      if (e.name == "TokenExpiredError") {
        res.status(401).send(e)
        return
      }
    }
  } else {
    req.user = {
      uid : "public",
      groups : []
    }
  }
  next()
})

app.get('/ping', (req, res) => {
  res.status(200).send("pong")
})

app.get('*/:piece/positions', (request, response) => {
  console.log("get positions", request.params.piece );
  corpus.positions(request.params[0], request.params.piece, request.user, (err, data) => {
    respond(response, data, err)
  })
})

app.get('*/:piece/analyses', (request, response) => {
  console.log("get analyses", request.params.piece);
  corpus.analyses(request.params[0], request.params.piece, request.user, (err, data) => {
    respond(response, data, err)
  })
})

app.get('*/:piece/analyses/:name', (request, response) => {
  console.log("get analysis", request.params.piece, request.params.name);
  corpus.analysis(request.params[0], request.params.piece, request.params.name, request.user, (err, data) => {
    respond(response, data, err, true)
  })
})


app.get('*/:id/recursive', (request, response) => {
  console.log("get piece or corpus recursive", request.params[0], request.params.id);
  corpus.corpus(request.params[0], request.params.id, request.user, (err, data) => {
    respond(response, data, err)
  }, true)
})

app.get('*/:id', (request, response) => {
  console.log("get piece or corpus", request.params[0], request.params.id);
  corpus.corpus(request.params[0], request.params.id, request.user, (err, data) => {
    respond(response, data, err)
  })
})

app.post('*/:id/analyses', (request, response) => {
    console.log("NEW ANALYSIS -> ", request.body.name, request.body.content, request.params[0]);
    corpus.createAnalysis(request.params[0], request.params.id, request.body.name, request.body.content, request.user, (err, data) => {
        respond(response, data, err)
    })
})

console.log('Starting server on port ' + PORT)
app.listen(PORT);
