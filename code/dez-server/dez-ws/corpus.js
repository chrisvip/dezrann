let fs = require('fs');

function canAccess (access, principal, permission) {
  return access[permission].includes('public')
    || access[permission].includes(principal.uid)
    || principal.groups.filter(g => access[permission].includes(g)).length > 0
}

function can (verb, principal, path, filename = "") {
  if (path == "") return false
  if (principal.groups.includes("admin")) return true
  try {
    let access = JSON.parse(fs.readFileSync(path + '/access.json', 'utf8'))
    if (filename != "" && access[filename]) {
      return canAccess(access[filename], principal, verb)
    } else if (access[verb]) {
      return canAccess(access, principal, verb)
    } else {
      return can(verb, principal, parentPath(path))
    }
  } catch (e) {
    if (e.code == "ENOENT") {
      return can(verb, principal, parentPath(path))
    } else {
      throw e
    }
  }
}

function parentPath (path) {
  let split = path.split('/')
  let parentLength = split.length - 1
  if (split[split.length - 1] == "") {
    parentLength = split.length - 2
  }
  return split.slice(0, parentLength).join("/")
}

function formatePath (path) {
  return path?path.slice(1) + "/":""
}

class Corpus {

  static _getAccessFileContent (path) {
    if (path == "") return []
    try {
      return JSON.parse(fs.readFileSync(path + '/access.json', 'utf8'))
    } catch (e) {
      if (e.code = "ENOENT") {
        return Corpus._getAccessFileContent(parentPath(path))
      }
    }
  }

  static _getAccessOf (path, filename = "") {
    let access = Corpus._getAccessFileContent(path)
    if (filename != "") {
      if (access[filename]) {
        access = access[filename]
      } else {
        access = Corpus._getAccessFileContent(parentPath(path))
        console.log(filename, path, parentPath(path), access);
      }
    }
    return Object.keys(access).map(key => {
      return {
        verb : key,
        subjects : access[key]
      }
    })
  }

  static _hasAccess(principal, access) {
    return access.subjects.includes("public")
      || access.subjects.includes(principal.uid)
      || principal.groups.filter(g => access.subjects.includes(g)).length > 0
  }

  static _getPermissions (principal, path) {
    if (principal.groups.includes("admin")) return ["all"]
    let accesses = Corpus._getAccessOf(path)
    let ret = []
    accesses.forEach (access => {
      if (Corpus._hasAccess(principal, access)) {
        ret.push(access.verb)
      }
    })
    return ret
  }

  static _getPieceData (path, id, principal) {
    if (can('read', principal, path + id)) {
      let piece = JSON.parse(fs.readFileSync(path + id + '/info.json', 'utf8'))
      if (can('read-access', principal, path + id)) {
        piece["access"] = Corpus._getAccessOf(path + id)
      }
      piece["permissions"] = Corpus._getPermissions(principal, path + id)
      return piece
    } else {
      throw "FORBIDDEN"
    }
  }

  static _getCorpusData (path, principal, recursive = false) {
    if (!can('read', principal, path)) return ""
    let dirs = fs.readdirSync(path)
    let pieces = []
    let corpora = []
    for (let dir of dirs) {
      if (dir == "access.json") continue
      try {
        let piece = Corpus._getPieceData(path, dir, principal)
        if (!piece.hide) pieces.push(piece)
      } catch (e) {
        try {
          if (can('read', principal, path + dir)) {
            if (recursive) {
              corpora.push(Corpus._getCorpusData(path + dir + "/", principal, recursive))
            } else {
              let corpus = {
                name : dir
              }
              if (can('read-access', principal, path + dir)) {
                corpus["access"] = Corpus._getAccessOf(path + dir)
              }
              corpora.push(corpus)
            }
          } else {
            throw "FORBIDDEN"
          }
        } catch (e) {
          if (e.code == "ENOTDIR") {
          } else if (e == "FORBIDDEN") {
          } else {
            throw e
          }
        }
      }
    }
    let ret = { id: path.split("/").reverse()[1] }
    if (pieces.length > 0) ret["pieces"] = pieces
    if (corpora.length > 0) ret["corpora"] = corpora
    return ret
  }

  static corpus(path, id, user, sendData, recursive) {
    let formatedPath = formatePath(path)
    let principal = user?user:{"uid": "public", "groups": []}
    try {
      try {
        sendData(undefined, Corpus._getPieceData(formatedPath, id, principal))
      } catch (e) {
        if (e.code == "ENOENT") {
          sendData(undefined, Corpus._getCorpusData(formatedPath + id + "/", principal, recursive))
        } else if (e == "FORBIDDEN") {
          sendData(e, undefined)
        } else {
          throw e
        }
      }
    } catch (err) {
      sendData(err, undefined)
    }
  }

  static positions(path, id, user, sendData) {
    if (!can('read', user, formatePath(path) + id)) {
      sendData(undefined, "")
      return
    }
    fs.readFile(formatePath(path) + id + '/positions.json', 'utf8', (err, data) => {
      if (err) {
        sendData(err, data)
      } else {
        try {
          sendData(err, JSON.parse(data))
        } catch (e) {
          sendData(e, data)
        }
      }
    })
  }

  static analyses(path, id, user, sendData) {
    let analysesDir = formatePath(path) + id + '/analyses'
    let principal = user?user:{"uid": "public", "groups": []}
    if (!can('read', principal, analysesDir)) {
      sendData(undefined, "")
      return
    }
    fs.readdir(analysesDir, (err, files) => {
      if (files == undefined) {
        sendData(undefined, "")
        return
      }
      let dezFiles = files
      .filter(f => f.endsWith(".dez"))
      .filter(f => can('read', principal, analysesDir, f))
      if (err) {
        sendData(err, dezFiles)
      } else {
        sendData(err, dezFiles)
      }
    });
  }

  static analysis (path, id, name, user, sendData) {
    let analysesDir = formatePath(path) + id + '/analyses/'
    let principal = user?user:{"uid": "public", "groups": []}
    if (!can('read', principal, analysesDir, name)) {
      sendData(undefined, "")
      return
    }
    fs.readFile(analysesDir + name, 'utf8', (err, data) => {
      if (err) return
      let analysis = JSON.parse(data)
      if (can('read-access', principal, analysesDir, name)) {
        analysis["access"] = Corpus._getAccessOf(analysesDir, name)
      }
      sendData(err, JSON.stringify(analysis, null, 2))
    })
  }

  static createAnalysis (path, id, name, content, user, sendData) {
    if (
      name.includes("reference.dez") ||
      name.includes("computed.dez") ||
      name == "algomus.dez" ||
      name == "empty.dez"
    ) {
      console.log("Dezrann samples can not be modified");
      return
    }
    if (!can('update-analysis', user, path.slice(1) + '/' + id + '/analyses/')) {
      sendData("FORBIDDEN", "")
      return
    }
    let dir = path.slice(1) + '/' + id + '/analyses/';
    if (fs.readdirSync(dir).length < 15) {
      let fileName = dir + name;
      fs.writeFile(fileName, content, 'utf8', (err, data) => {
        if (err) {
          sendData(err, data)
        } else {
          sendData(err, "file " + fileName + "created");
        }
      })
    } else {
      console.log("number of analysis exceded");
    }
  }

}

module.exports = Corpus;
