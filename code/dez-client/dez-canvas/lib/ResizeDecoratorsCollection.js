class ResizeDecoratorsCollection {

    constructor (view) {
        this._resizeDirections = ["LEFT", "RIGHT"];
        this._resizeDecorators = [];
        this._view = view;
    }

    margin (direction) {
        let ret = this._resizeDecorators
            .filter(decorator => decorator.direction == direction)
            .reduce((width, decorator) => width + decorator.value.width, 0)
        return ret;
    }

    update (rect) {
        this._resizeDecorators.forEach(
            decorator => decorator.value.redraw(
                rect.middle(decorator.direction)
            )
        )
    }

    decorate (rect) {
        this._resizeDecorators = this._resizeDirections.map(
            direction => {
                return {
                    direction: direction,
                    value: this._view.createResizeDecorator(
                        direction,
                        rect.middle(direction)
                    )
                }
            }
        )
    }

    deleteAll () {
        this._resizeDecorators.forEach(decorator => decorator.value.remove())
        this._resizeDecorators = []
    }

    get hidden () { return this._resizeDecorators == 0; }

    contains (x) {
        return this._resizeDecorators
            .filter(decorator => decorator.value.contains(x), false)
            .length > 0;
    }

}
