class Triangle extends Shape {

    constructor (view, resizers, defaults = {}) {
        super(view, resizers, defaults)
        if (defaults && defaults["text"]) {
            this._DEFAULT_TEXT = defaults["text"];
        } else {
            this._DEFAULT_TEXT = "triangle"
        }
        this._viewTrngl == undefined // created in show (...)

        this._MARGIN_WIDTH = -20
        this._MARGIN_HEIGHT = -1
        this._selected = false;

        this.direction = undefined;

        this._canBeOnSeveralLines = defaults.canBeOnSeveralLines;
        this._canBeOnAllLines = defaults.canBeOnAllLines;
    }

    _setFlag (flag, defaultValue) {
        if (flag == undefined) {
            flag = defaultValue;
        }
        return flag;
    }

    get canBeOnSeveralLines () {
        return this._setFlag(this._canBeOnSeveralLines, true);
    }

    get canBeOnAllLines () {
        return this._setFlag(this._canBeOnAllLines, false);
    }

    show (x, y, width, height) {
        if (this._viewTrngl == undefined) {
            this._viewTrngl = this._view.createTriangle(
                x, y, width, height, this.direction, this._color, this._OPACITY
            )
        } else {
            this._viewTrngl.redraw(x, y, width, height)
        }
        if (this._text == undefined) {
            this.text = this._DEFAULT_TEXT
        }
        this.$placeText()
    }

    $placeText() {
        if (this._viewTrngl.canContain(this._text.box, this._MARGIN_WIDTH, this._MARGIN_HEIGHT)) {
            let x = this._viewTrngl.x - this._text.box.width / 2;
            let y = this._viewTrngl.y;
            if (this.direction == "up")
            {
                y = y + this._viewTrngl.height + this._text.box.height / 2 ;
            }
            else {
                y = y + this._MARGIN_HEIGHT ;
            }
            this._text.moveTo(x, y);
        } else {
            this._text.hide();
        }
    }

    remove () {
        super.remove()
        if (this._viewTrngl != undefined) {
            this._viewTrngl.remove();
        }
    }

    showAsSelected () {
        this._selected = true;
        this._viewTrngl.strokeWidth = 2;
    }

    showAsUnselected () {
        this._selected = false;
        this._viewTrngl.strokeWidth = 0;
    }

    contains (x) {
        if (this._viewTrngl == undefined) return false;
        return this._viewTrngl.contains(x);
    }

}
