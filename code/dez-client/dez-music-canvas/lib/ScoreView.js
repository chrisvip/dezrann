class ScoreView {

    constructor (canvas, map, labelTypes) {
        this._canvas = canvas;
        this._map = map;
        this._labelTypes = labelTypes;
        this._toLabelId = {}
        this._toGrobId = {}
    }

    set labelTypes (labelTypes) {
      this._labelTypes = labelTypes
    }

    set map (map) {
        this._map = map
    }

    _id2index (id) {
        return parseInt(id.split('.')[1])
    }

    _id2group(id) {
        return id.split('.')[0]
    }

    _line (label) {
        if (label.staff) return "staves." + label.staff
        if (label.line) return label.line;
        return this._labelTypes.defaultLineOf(label.type) || 'bot.3'
    }

    remove (labelId) {
        this._canvas.deleteGrob(this._toGrobId[labelId]);
    }

    draw (label) {
        if (label.duration == undefined) label.duration = 0;
        let id = this._canvas.add({
            x: this._map.toGraphicalX(label.start),
            width: this._map.toGraphicalLength(label.start, label.duration),
            text: label.tag,
            color: this._labelTypes.colorOfType({
              type: label.type,
              tag: label.tag
            }).hex,
            shape: label.duration==0?"VerticalLine":"Rectangle"
        })
        .onLine(label.line)
        .getId();
        this._toLabelId[id] = label.id;
        this._toGrobId[label.id] = id;
    }

    redraw (label) {
        this._canvas.updateGrob({
            id: this._toGrobId[label.id],
            text: label.tag == undefined ? "" : label.tag,
            color: this._labelTypes.colorOfType({
              type: label.type,
              tag: label.tag
            }).hex,
            width: this._map.toGraphicalLength(label.start, label.duration),
            x: this._map.toGraphicalX(label.start),
            line: label.line
        })
    }

    selected (label) {
      this._canvas.selectGrob(this._toGrobId[label.id])
    }

    associateIds (labelId, grobId) {
      this._toLabelId[grobId] = labelId;
      this._toGrobId[labelId] = grobId;
    }

    toLabel (grob) {
        let label = {
            id: this._toLabelId[grob.id]
        }
        this.updateLabel(label, grob)
        return label;
    }

    updateLabel(label, grob) {
        label.start = this._map.toMusicalOnset(grob.x);
        label.duration = this._map.toMusicalDuration(grob.x, grob.width);
        label.tag = grob.text;
        if (grob.linesIds.length == 1) {
            label.line = grob.linesIds[0]
            if (this._id2group(label.line) == "staves") {
              label.staff = this._id2index(label.line)
            }
        } else { // 0 or several
            label.line = "all";
        }
    }

    resetLabels() {
        this._canvas.deleteGrobs();
    }

    deleteLines() {
        this._canvas.deleteLines();
    }
}
