class Analysis {

  constructor () {
    this.erase()
    // ! HARD CODED !
    this._linesNames = [
      "staves.1",
      "top.3",
      "top.2",
      "top.1",
      "bot.1",
      "bot.2",
      "bot.3"
    ]
  }

  erase () {
    this._meta = {}
    this._labels = []
  }

  get size () {
    return this._labels.length
  }

  get labels () {
    // return a copy not the private object
    return this._labels.filter(label => true)
  }

  updateMeta (meta) {
    // update the meta information with other information
    Object.keys(meta).forEach(key => {
      this._meta[key] = meta[key]
    })
  }

  addLabel (label) {
    if (label.line == undefined) {
      if (label.staff != undefined) {
        label.line = "staves." + label.staff.toString()
      } else {
        label.line = this._mostFreeLineAt(label.start, label.duration)
        if (label.line.startsWith("staves")) {
          label.staves = parseInt(label.line.split('.')[1])
        }
      }
    }
    this._labels.push(label);
  }

  _mostFreeLineAt (start, duration) {
    // TO OPTIMIZE
    for (let lineName of this._linesNames) {
      let overlaps = this._labels
      .filter(label => label.line == lineName)
      .filter(label => label.start <= start && start <= label.start + label.duration)
      .length
      if (overlaps == 0) return lineName
    }
    return "staves.1"
  }

  get usedTags () {
    // OPTIMIZE: set list only on label modification
    let ret = []
    this._labels.forEach(label => {
      let tag = ret.filter(t => t.name == label.tag)[0]
      ret = ret.filter(t => t.name != label.tag)
      ret.push({
        name : label.tag,
        occurences : tag != undefined?tag.occurences + 1:1
      })
    })
    return ret
  }

  getLabelById (id) {
    let copy = {}
    let label = this._labels.filter(l => l.id == id)[0]
    if (label) {
      Object.keys(label).forEach(key => {
        copy[key] = label[key]
      })
      copy.end = label.end
      return new Label(copy)
    } else {
      return undefined
    }
  }

  removeLabel (id) {
    this._labels = this._labels.filter(l => l.id != id)
  }

  updateLabel (label) {
    let savedLabel = this.getLabelById(label.id)
    Object.keys(label).forEach(key => {
      if (label[key] != undefined) {
        savedLabel[key] = label[key]
      }
    })
    if (savedLabel.line.substring(0,6) != "staves") {
      delete savedLabel.staff
    }
    this.removeLabel(label.id)
    this.addLabel(savedLabel)
  }

  toString (sortedGroups = []) {
    let ret = "";

    // From https://stackoverflow.com/a/34890276
    function groupBy(xs, key) {
      return xs.reduce((rv, x) => {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    }

    let labelsLine = groupBy(this._labels, 'line')

    function lineToString (line) {
      return line + ": "
      + labelsLine[line]
      .sort((a,b) => a.start - b.start)
      .map(label => label.toString())
      .join(', ') + "\n"
    }

    sortedGroups.forEach(
      group => Object.keys(labelsLine)
      .filter(line => line.startsWith(group))
      .sort()
      .forEach(line => ret += lineToString(line))
    )

    Object.keys(labelsLine)
    .filter(
      line => !sortedGroups.reduce(
        (acc, group) => acc?acc:line.startsWith(group),
        false)
      )
      .sort()
      .forEach(line => ret += lineToString(line))

      return ret;
  }

  labelsToExport () {
    return this._labels.map(label => {
      let item = Object.assign({}, label)
      if (typeof item.layer !== 'undefined') delete item.layer
      if (item.duration == 0) delete item.duration
      if (!item.tag || item.tag.length == 0) delete item.tag
      if (!item.comment || item.comment.length == 0) delete item.comment
      if (typeof item.line === 'undefined' || item.line.startsWith('staves')) {
        if (typeof item.staff === 'undefined') {
          item.staff = parseInt(item.line.split('.')[1])
        }
        delete item.line
      } else {
        delete item.staff
      }
      return item
    })
  }

  fieldsToExport () {
    return [
      'type', 'start', 'duration',
      'staff', 'line',
      'tag', 'comment'
    ]
  }

  toJson () {
    this._meta["date"] = (new Date()).toISOString()
    this._meta["producer"] = "Dezrann c6c80150"

    let ret = {
      "meta": this._meta,
      "labels": "LABELS"
    }
    let labelsJsonRaw = this.labelsToExport().map(item => {
      // No indentation here, we keep each label on a single line
      return JSON.stringify(item, this.fieldsToExport(), " ").replace(/\n/g, "")
    })

    let indent = '    '
    let labelsJson = '[\n' + indent + indent + labelsJsonRaw.join(',\n' + indent + indent) + '\n' + indent + ']'
    return JSON.stringify(ret, null, indent).replace('"LABELS"', labelsJson)
  }

  toCSV () {
    let fields = this.fieldsToExport()
    return fields.join(";")
    + "\n"
    + this.labelsToExport().map(item => fields
      .map(field => item[field])
      .join(';')
    ).join('\n')
  }

}
