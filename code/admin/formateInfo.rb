# print info.json file (put in argv) into the correct format
require 'json'

file = File.open ARGV[0]
data = JSON.load file
new = data.dup
images = new["sources"]
new["sources"] = {}
new["sources"]["images"] = images
puts JSON.pretty_generate(new)
