#!/bn/bash

# corpus must be in a directory called 'corpus'
# very temporary script...

# correct positions files
for file in `grep -r \"notes\": corpus|awk -F':' '{print $1}'`; do
  sed 's/\"notes\":/\"onset-x\":/g' $file > positions.json;
  cp positions.json $file;
done

# correct info.json files
for file in `grep -r "\"sources\":\ \[" corpus| awk -F':' '{print $1}'`; do
  echo $file; ruby formateInfo.rb $file > info.json;
  cp info.json $file;
done
