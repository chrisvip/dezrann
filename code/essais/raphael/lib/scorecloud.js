// Fonctions orphelines
function toOffset(x) {
  return x / 100;
}
function labelsToSting (labels) {
  var s = "";
  for (var i = 0; i < labels.length; i++) {
    s += i;
    for (var key in labels[i]) {
      s += "(" + labels[i][key].start + "," + labels[i][key].duration + ")";
    }
  }
  return s;
}

function getLabel(labels, id) {
  for (var i = 0; i < labels.length; i++) {
    for (var key in labels[i]) {
      if (key == id) {
        return labels[i][key];
      }
    }
  }
  return null;
}

// Schema
function Label (start, duration) {
  this.start = start;
  this.duration = duration;
}

function Schema (nbStaffs) {
  var labels = [];
  for (var i = 0; i < nbStaffs; i++) {
    labels[i] = {};
  };
  this.addLabel = function (staffIndex, id, label) {
    labels[staffIndex][id] = label;
  };
  this.getLabel = function (id) {
    for (var i = 0; i < labels.length; i++) {
      for (var key in labels[i]) {
        if (key == id) {
          return labels[i][key];
        }
      }
    }
    return null;
  };
  this.toString = function () {
    var s = "";
    for (var i = 0; i < labels.length; i++) {
      s += i;
      for (var key in labels[i]) {
        s += "(" + labels[i][key].start + "," + labels[i][key].duration + ")";
      }
    }
    return s;
  };

}

// Score Image

function Score (nbStaffs) {
  var staffs = [];
  for (var i = 0; i < nbStaffs; i++) {
    staffs[i] = new Staff();
  };
  this.add = function (staff, index) {
    staffs[index] = staff;
  };
  this.getStaff = function (index) {
    return staffs[index];
  };
}

function Staff () {
  var labels = [];
  this.add = function (label, id) {
    labels[id] = label;
  };
}

function Glab (raphRect) {
  var raphRect =
    raphRect
      .attr ({
                fill: "#f00",
                stroke: "#fff",
                opacity: "0.2"
              });
  this.resize = function (width) {
    raphRect.attr({width: width});
  };
  this.id = raphRect.id;
  this.setBehavior = function (observer) {
    raphRect.drag (
      function (dx, dy, x, y, event) {
        if (this.command === "scaleright") {
          var newwidth = this.owidth + dx;
          if (newwidth > 50) {
            this.attr({width: newwidth});
            observer.fire('scaleright', {id: this.id, width: newwidth});
          }
        } else if (this.command === "scaleleft"){
          var newwidth = this.owidth - dx;
          if (newwidth > 50) {
            var x = this.ox + dx;
            this.attr({width: newwidth, x: x});
            observer.fire('scaleleft', {id: this.id, x: x, width: newwidth});
          }
        } else { // drag
          x = this.ox + dx;
          this.attr({x: x});
          observer.fire('drag', {id: this.id, x: x});
        }
        event.stopPropagation();
      },
      function (x, y, event) {
        if ( x - this.attr("x") < this.attr("width")/4 ) {
          this.command = "scaleleft";
        } else if ( x - this.attr("x") > 3*this.attr("width")/4 ) {
          this.command = "scaleright";
        } else {
          this.command = "drag"
        }
        this.owidth = this.attr("width");
        this.ox = this.attr("x");
        event.stopPropagation();
      }
    );
  };
}

function Gstaff (top, bottom, raphPaper, index) {
  var raphPaper = raphPaper;
  this.top = top;
  this.bottom = bottom;
  this.index = index;
  this.createGlabOn = function (x) {
    var newrect = raphPaper.rect(x-15, top, 0, 50, 10);
    return new Glab(newrect);
  }

}

function ScoreImage (id) {
  var id = id;
  var raphPaper = Raphael(
    id, "100%", 210
  );
  var currentStaffIndex = 0;
  var raphImage = raphPaper.image(
    '/images/02.png', 0,0, 8762/2, 477/2
  ).drag();
  var staffsPos = [
    {"top": 25, "bottom": 75},
    {"top": 85, "bottom": 135},
    {"top": 155, "bottom": 205}
  ];
  this.getStaffAt = function (y) {
    var rely = window.pageYOffset + y - document.getElementById(id).offsetTop;
    for (var i = 0; i < staffsPos.length; i++) {
      var top = staffsPos[i].top;
      var bottom = staffsPos[i].bottom;
      if (rely > top && rely < bottom) {
        currentStaffIndex = i;
        return new Gstaff(top, bottom, raphPaper, currentStaffIndex);
      }
    }
  };
  this.currentStaffIndex = function () {
    return currentStaffIndex;
  };
}
