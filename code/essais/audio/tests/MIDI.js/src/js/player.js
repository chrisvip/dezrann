/**
 * 
 */
class player {

    /**
     * Permet d'initialiser une instance du type player à partir d'un
     * dictionnaire d'options.
     * 
     * @param   {Object} options
     */
    constructor(options) {

        this.currentTime = options.currentTime

        /* TODO */
    }

    /**
     * Charge une fichier MIDI dont la chemin d'accès est passé en paramètre.
     * 
     * @param   {String} path
     */
    load(path) {

        /* TODO */
    }

    /**
     * Commence la lecture du fichier MIDI.
     */
    play() {

        /* TODO */
    }

    /**
     * Stoppe la lecture du fichier MIDI.
     */
    stop() {

        /* TODO */
    }

    /**
     * Met en pause la lecture du fichier MIDI.
     */
    pause() {

        /* TODO */
    }

    /**
     * Rend muet la lecture du canal passé en paramètre.
     * 
     * @param   {Number} channel
     */
    mute(channel) {

        /* TODO */
    }

    /**
     * Rend muet tous les canaux ormis celui passé en paramètre.
     * 
     * @param   {Number} channel 
     */
    solo(channel) {

        /* TODO */
    }

    /**
     * Déplace le curseur courrant au temps passé en paramètre.
     * 
     * @param {String} time
     */
    setTime(time) {

        /* TODO */
    }
}

