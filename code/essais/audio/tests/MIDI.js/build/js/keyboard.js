"use strict";

var onMIDIload = function onMIDIload() {

    MIDI.setVolume(0, 127);

    function play() {

        var note = parseInt(this.getAttribute("data-note"));

        MIDI.noteOn(0, note, 127, 0);
        MIDI.noteOff(0, note, 0.5);
    }

    var notes = document.querySelectorAll(".note");

    for (var i = 0; i < notes.length; i++) {

        notes.item(i).addEventListener("click", play);
    }
};

var setupEventListener = function setupEventListener() {

    var keyboardContainer = document.querySelector(".keyboard-container");

    var note = 41;

    for (var i = 0; i < 35; i++) {

        if ([7, 13, 21, 27].indexOf(i) != -1) continue;

        var t = document.createElement("div");

        var j = Math.floor(i / 2);

        if (i % 2 == 0) {

            t.style.left = j * 50.5 + "px";
            t.setAttribute("class", "note touche-blanche");
            t.setAttribute("data-note", note++);
        } else {

            t.style.left = j * 50.5 + 35 + "px";
            t.setAttribute("class", "note touche-noire");
            t.setAttribute("data-note", note++);
        }

        keyboardContainer.appendChild(t);
    }

    MIDI.loadPlugin({
        soundfontUrl: "bower_components/midi/examples/soundfont/",
        instrument: "acoustic_grand_piano",
        onprogress: function onprogress(state, progress) {
            console.log(state, progress);
        },
        onsuccess: onMIDIload
    });
};

window.addEventListener("load", setupEventListener);