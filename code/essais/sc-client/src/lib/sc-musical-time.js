export class MusicalTime {

    /**
     * Initializes an instance of MusicalTime with the number of beats per
     * minute, and the number of beats per bar.
     *
     * @param {number} bpm Beats per minute
     * @param {number} bpb Beats per bar
     */
    constructor(bpm = 120, bpb = 4) {

        this.bpm = bpm
        this.bpms = this.bpm / (60 * 1000)
        this.bpb = bpb
    }

    /**
     * Converts a time in millisecond to a musical time.
     *
     * @param {number} ms Time in millisecond
     */
    toMusicalTime(ms) {

        return ms * this.bpms
    }

    /**
     * Converts a musical time to a time in millisecond.
     *
     * @param {number} mt Musical time
     */
    toMillisecond(mt) {

        return mt / this.bpms
    }

    /**
     * Returns the first time of a measure from a muscial time.
     *
     * @param {number} mt Muscial time
     */
    firstTimeOfMeasure(mt) {

        return Math.floor(mt / this.bpb) * this.bpb
    }
}
