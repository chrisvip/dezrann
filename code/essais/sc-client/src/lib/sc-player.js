/**
 * Object type released to synchronize graphic interface and sound player.
 */
class Position {

    /**
     * Constructs a new position with the current time and the ending time.
     *
     * @param {number} now
     * @param {number} end
     */
    constructor(now, end) {

        this.now = now
        this.end = end
    }
}

/**
 * Object in charge to load, play and control a MIDI file.
 */
export class MIDIPlayer {

    constructor() {

        this._listeners = []
    }

    /**
     * Initializes the MIDI player.
     */
    init() {
        return new Promise((resolve, reject) => {

            MIDI.Player.addListener(status => {

                let position = new Position(status.now, status.end)

                for (let listener of this._listeners)
                    listener(position)
            })

            MIDI.loadPlugin({
                soundfontUrl: "/bower_components/midi/examples/soundfont/",
                instrument: "acoustic_grand_piano",
                onsuccess: resolve,
                onprogress: null,
                onerror: reject
            })
        })
    }

    /**
     * Loads a file with a specific bpm (beats per minute).
     *
     * @param {string} midi Path to MIDI file
     * @param {number} bpm
     */
    load(midi, bpm = 120) {

        return new Promise((resolve, reject) => {

            let onsuccess = () => {

                for (let i = 0; i < 15; i++)
                    MIDI.channels[i].instrument = 0

                resolve()
            }

            MIDI.Player.BPM = bpm

            MIDI.Player.loadFile(midi, onsuccess, null, reject)
        })
    }

    /**
     * Adds a function called every played musical note.
     * @param {function} listener
     */
    addListener(listener) {

        this._listeners.push(listener)
    }

    /**
     * Removes a listening function.
     *
     * @param {function} listener
     */
    removeListener(listener) {

        let i = this._listeners.findIndex(e => e === listener)

        if (i > -1)
            this._listeners.splice(i, 1)
    }

    /**
     * Starts playing.
     */
    play() {

        MIDI.Player.start()
    }

    /**
     * Changes the current time.
     *
     * @param {number} time Current time in millisecond
     */
    setTime(time) {

        MIDI.Player.currentTime = time
        this.play()
    }

    /**
     * Stops temporary playing.
     */
    pause() {
        MIDI.Player.pause()
    }

    /**
     * Stops playing
     */
    stop() {
        MIDI.Player.stop()
    }

    /**
     * Plays a sample from a start position and a duration.
     *
     * @param {number} start
     * @param {number} duration
     */
    sample(start, duration) {

        let self = this
        let onPlay = function (status) {

            if (status.now > start + duration) {

                self.removeListener(onPlay)
                self.pause()
            }
        }

        this.addListener(onPlay)

        this.setTime(start)
    }

    /**
     * Mutes a specific channel
     *
     * @param {number} channel
     */
    mute(channel) {

        MIDI.channels[channel].mute = !MIDI.channels[channel].mute
        this.play()
    }

    /**
     * Plays solo a specific channel
     *
     * @param {number} channel
     */
    solo(channel) {

        MIDI.channels[channel].solo = !MIDI.channels[channel].solo
        this.play()
    }
}

const MP3 = {}

/**
 * Object in charge to load, play and control a MP3 file.
 */
export class MP3Player {

    constructor() {

        this._listeners = []
    }

    /**
     * Initializes the MP3 player.
     */
    init() {
        return new Promise((resolve, reject) => {


            MP3.context = new AudioContext()
            MP3.analyser = MP3.context.createAnalyser()
            MP3.source
            MP3.Player = new Audio()

            MP3.Player.ontimeupdate = () => {

                let position = new Position(MP3.Player.currentTime * 1000, MP3.Player.duration * 1000)

                for (let listener of this._listeners)
                    listener(position)
            }

            resolve()
        })
    }

    /**
     * Loads a MP3 file.
     *
     * @param {string} mp3
     */
    load(mp3) {

        return new Promise((resolve, reject) => {

            MP3.Player.src = mp3
            MP3.Player.controls = true
            MP3.Player.autoplay = false
            MP3.Player.loop = false
            MP3.source = MP3.context.createMediaElementSource(MP3.Player)
            MP3.source.connect(MP3.analyser)
            MP3.analyser.connect(MP3.context.destination)

            console.log(MP3)

            resolve()
        })
    }

    /**
     * Adds a function called every played musical note.
     * @param {function} listener
     */
    addListener(listener) {

        this._listeners.push(listener)
    }

    /**
     * Removes a listening function
     *
     * @param {function} listener
     */
    removeListener(listener) {

        let i = this._listeners.findIndex((e) => e === listener)

        if (i > -1)
            this._listeners.splice(i, 1)
    }

    /**
     * Starts playing.
     */
    play() {

        MP3.Player.play()
    }

    /**
     * Changes the current time.
     *
     * @param {number} time Current time in millisecond
     */
    setTime(time) {

        MP3.Player.currentTime = time / 1000
    }

    /**
     * Stops temporary playing.
     */
    pause() {
        MP3.Player.pause()
    }

    /**
     * Stops playing.
     */
    stop() {
        this.pause()
        MP3.Player.currentTime = 0
    }

    /**
     * Plays a sample from a start position and a duration.
     *
     * @param {number} start
     * @param {number} duration
     */
    sample(start, duration) {

        let self = this
        let onPlay = function (status) {

            if (status.now > start + duration) {

                self.removeListener(onPlay)
                self.pause()
            }
        }

        this.addListener(onPlay)

        this.setTime(start)
    }
}
