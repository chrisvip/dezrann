// Schema
export class Label {
    constructor (start, duration, type, tag = "", staff) {
        this.start = start;
        this.duration = duration;
        this.type = type;
        this.tag = tag;
        this.staff = staff;
    }
    toString () {
        let s = "";
        s += this.start
        if (this.duration)
            s += "[" + this.duration + "]";
        s += "," + this.type;
        if (this.tag)
            s += "," + this.tag;
        return s;
    };

    isEqualTo (label) {
        return  this.start == label.start &&
                this.duration == label.duration &&
                this.type == label.type &&
                this.tag == label.tag &&
                this.staff == label.staff
    }
}

export class Schema {
    constructor () {
        this._labels = {};
        this._nbStaffs = 0;
    }
    addLabel (id, label) {
        this._labels[id] = label;
        if (label.staff) {
            if (label.staff > this._nbStaffs) {
                this._nbStaffs = label.staff;
            }
        }
    };
    getLabel (id) {
        return this._labels[id];
    };
    toString () {
        let s = "";
        for (let i = 0; i < this._nbStaffs; i++) {
            s += i+1;
            for (let label of this.getLabelsByStaff(i+1)) {
                s += "(" + label.toString() + ")";
            }
            s += "\n"
        }
        s += "cadences -> "
        for (let label of this.getLabelsByType("Cad")) {
            s += "(" + label.toString() + ")";
        }
        s += "\n"
        return s;
    };
    getLabelsByStaff (staffNumber) {
        let ret = [];
        for (let key in this._labels) {
            let label = this._labels[key]
            if (label.staff == staffNumber) {
                ret.push(label)
            }
        }
        return ret;
    }
    getLabelsByType (type) {
        let ret = [];
        for (let key in this._labels) {
            let label = this._labels[key]
            if (label.type == type) {
                ret.push(label)
            }
        }
        return ret;
    }

    toJson () {
        let ret = {"labels":[]}
        for (let key in this._labels) {
            ret["labels"].push(this._labels[key])
        }
        return JSON.stringify(ret, null, '    ');
    };
    deleteLabel (id) {
        delete this._labels[id];
    };

}
