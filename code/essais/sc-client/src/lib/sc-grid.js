export class Grid {
    constructor () {
        // xpos must be ordered
        this.xpos = [];
        this.ratio = 1;
    };
    setRatio(r) {
        if (this.ratio != r) {
            this.ratio = r;
            for (let i = 0; i < this.xpos.length; i++) {
                this.xpos[i] = Math.round(this.xpos[i]*this.ratio);
            }
        }
    }
    setPositions (notespos) {
        for (let i = 0; i < notespos.length; i++) {
            let x = notespos[i]["x"];
            let xr = x * this.ratio;
            let round = Math.round(xr);
            this.xpos[i] = Math.round(notespos[i]["x"]*this.ratio);
        }
    };
    _getIndex(x) {
        let index = 0
        while (this.xpos[index] != x && index < this.xpos.length) {
            index++;
        }
        if (index == this.xpos.length) {
            throw "x out of range";
        }
        return index
    }
    nextStep (x) {
        let index = this._getIndex(x);
        if (index != this.xpos.length-1) {
            return this.xpos[index+1];
        } else {
            return this.xpos[index]
        }
    }
    previousStep (x) {
        let index = this._getIndex(x);
        if (index != 0) {
            return this.xpos[index-1];
        } else {
            return this.xpos[index]
        }
    }
    approxInf (x) {
        let index = 0
        while (this.xpos[index] <= x && index < this.xpos.length) {
            index++;
        }
        if (index == this.xpos.length || index == 0) {
            throw "x out of range";
        }
        return this.xpos[index-1]
    }
    approxSup (x) {
        let index = 0
        while (this.xpos[index] <= x && index < this.xpos.length) {
            index++;
        }
        if (index == this.xpos.length) {
            throw "x out of range";
        }
        return this.xpos[index]
    }
    leftEdge () {
        return this.xpos[0];
    }
    margin () {
        return Math.round(10*this.ratio);
    }

}
