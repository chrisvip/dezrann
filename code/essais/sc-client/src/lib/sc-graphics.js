class draggableRectangle {

    constructor (grid) {
        this._grid = grid;
        this.draggable = true;
    }

    _getDragCommand (x) {
        let xInRect = x - this.getX();
        if ( xInRect < this.getWidth()/4 ) {
            return "scaleleft";
        } else if ( xInRect > 3*this.getWidth()/4 ) {
            return "scaleright";
        } else {
            return "drag"
        }
    }

    dragStart (x) {
        this._dragCommand = this._getDragCommand (x);
        this._ox = this.getX();
        this._owidth = this.getWidth();
        this._leftRectEdgeNextStep = this._grid.nextStep(this._ox);
        this._leftRectEdgePreviousStep = this._grid.previousStep(this._ox);
        this._rightRectEdgeNextStep = this._grid.nextStep(this._ox + this._owidth);
        this._rightRectEdgePreviousStep = this._grid.previousStep(this._ox + this._owidth);
        this._lastdx = 0;
    }

    dragMove (dx) {
        let actualRectLeft = this.getX();
        let actualRectRight = actualRectLeft + this.getWidth();
        let proposedRectRight = this._ox + this._owidth + dx;
        let proposedRectLeft = this._ox + dx;
        let newwidth;
        let newx;
        let direction = (this._lastdx < dx)?"right":"left";
        let newRight;
        this._lastdx = dx;
        switch (this._dragCommand) {
            case "scaleright":
                if (direction == "right" && proposedRectRight >= this._rightRectEdgeNextStep) {
                    newRight = this._grid.approxInf(proposedRectRight);
                } else if (direction == "left" && proposedRectRight <= this._rightRectEdgePreviousStep) {
                    newRight = this._grid.approxSup(proposedRectRight);
                }
                if (newRight) {
                    newwidth = newRight - this._ox;
                    this._rightRectEdgeNextStep = this._grid.nextStep(newRight);
                    this._rightRectEdgePreviousStep = this._grid.previousStep(newRight);
                    if (newwidth > 50) {
                        this.resize(newwidth);
                    }
                }
                break;
            case "scaleleft":
                if (direction == "right" && proposedRectLeft >= this._leftRectEdgeNextStep) {
                    newx = this._grid.approxInf(proposedRectLeft);
                } else if (direction == "left" && proposedRectLeft <= this._leftRectEdgePreviousStep && actualRectLeft > this._grid.leftEdge()) {
                    newx = this._grid.approxSup(proposedRectLeft);
                }
                if (newx) {
                    this._leftRectEdgeNextStep = this._grid.nextStep(newx);
                    this._leftRectEdgePreviousStep = this._grid.previousStep(newx);
                    newwidth = this._owidth + this._ox - newx;
                    if (newwidth > 50) {
                        this.moveTo(newx);
                        this.resize(newwidth);
                    }
                }
                break;
            case "drag":
                if (direction == "right") {
                    if (proposedRectLeft >= this._leftRectEdgeNextStep || proposedRectRight >= this._rightRectEdgeNextStep) {
                        newRight = this._grid.approxInf(proposedRectRight)
                        newx = this._grid.approxInf(proposedRectLeft);
                    }
                }
                if (direction == "left" && actualRectLeft > this._grid.leftEdge()) {
                    if (proposedRectLeft < this._leftRectEdgePreviousStep || proposedRectRight < this._rightRectEdgePreviousStep) {
                        newRight = this._grid.approxSup(proposedRectRight)
                        newx = this._grid.approxSup(proposedRectLeft);
                    }
                }
                if (newRight) {
                    this._leftRectEdgeNextStep = this._grid.nextStep(newx)
                    this._leftRectEdgePreviousStep = this._grid.previousStep(newx)
                    this._rightRectEdgeNextStep = this._grid.nextStep(newRight)
                    this._rightRectEdgePreviousStep = this._grid.previousStep(newRight)
                    newwidth = newRight - newx;
                    this.moveTo(newx);
                    this.resize(newwidth);
                }
                break;
        }
    };

}

class Gglab extends draggableRectangle {

    constructor (id, rect, text, grid) {
        super(grid);
        this._gtext = text;
        this._rect =  rect;
        this._selected = false;
        this.id = id;
    }

    getX() {
        return parseInt(this._rect.attr("x"))
    }

    getWidth() {
        return parseInt(this._rect.attr("width"))
    }

    _centerText () {
        let middle = this.getWidth()/2 + this.getX();
        this._gtext.attr ({x : middle})
    }
    resize (width) {
        this._rect.attr({width: width});
        this._centerText()
    }

    moveTo (x) {
        this._rect.attr({x: x})
        this._centerText()
    }

    isSelected() {
        return this._selected
    }

    select () {
        this._rect.attr({opacity: 0.8});
        this._selected = true;
    }

    unselect () {
        this._rect.attr({opacity: 0.4});
        this._selected = false;
    }

    remove () {
        this._rect.remove();
        this._gtext.remove();
    }

    setColor (color) {
        this._rect.attr({fill: color});
    }

    setText (text) {
        this._gtext.attr ({text : text})
    }

}

class VerticalLine {

    constructor (id, rect, text, grid) {
        this.draggable = true;
        this.id = id;
        this._rect = rect;
        this._margin = Math.round(parseInt(this._rect.attr("width"))/2);
        this._rect.attr({x: parseInt(this._rect.attr("x")) - this._margin})
        this._text = text;
        this._grid = grid;
        this._textDeltaX = parseInt(this._text.attr("x")) - this.getX();
        this._selected = false;
    }

    getX() {
        return parseInt(this._rect.attr("x")) + this._margin
    }

    getWidth() {
        return 0;
    }

    moveTo (x) {
        this._rect.attr({x: x - this._margin})
        this._text.attr({x: x + this._textDeltaX})
    }

    dragStart (x) {
        this._ox = this.getX()
        this._textDeltaX = parseInt(this._text.attr("x")) - this._ox;
        this._leftStep = this._grid.previousStep(this._ox);
        this._rightStep = this._grid.nextStep(this._ox);
    }

    dragMove (dx) {
        let proposedX = this._ox + dx;
        let newx;
        if (proposedX >= this._rightStep) {
            newx = this._grid.approxInf(proposedX);
        } else if (proposedX <= this._leftStep) {
            newx = this._grid.approxSup(proposedX);
        } else {
            return;
        }
        this.moveTo(newx)
    }

    remove () {
        this._rect.remove();
        this._text.remove();
    }

    isSelected() {
        return this._selected
    }

    select () {
        this._rect.attr({opacity: 0.8});
        this._selected = true;
    }

    unselect () {
        this._rect.attr({opacity: 0.5});
        this._selected = false;
    }

    setText (text) {
        this._text.attr({text: text});
    }

    setColor (color) {
        this._rect.attr({fill: color});
    }
    resize (width) {}
}

class Glab extends draggableRectangle{

    constructor (id, raphRect, text, grid) {
        super(grid);
        this._raphRect = raphRect;
        this._gtext = text;
        this._raphRect.attr ({
            stroke: "black",
            strokeWidth:0,
            opacity: "0.4",
            id: id
        });
        this._margin = this._grid.margin();
        this._raphRect.attr ({
            x: parseInt(this._raphRect.attr("x")) - this._margin
        })
        this._selected = false;
        this.id = id;
    }

    getX () {
        return parseInt(this._raphRect.attr("x")) + this._margin
    }

    getWidth () {
        return parseInt(this._raphRect.attr("width")) - 2*this._margin
    }

    _placeText () {
        let height = parseInt(this._raphRect.attr("height"));
        this._gtext.attr ({x : this.getX() + Math.round(height/10)})
    }

    moveTo (x) {
        this._raphRect.attr({x: x - this._margin});
        this._placeText();
    }

    resize (width) {
        this._raphRect.attr({width: width + 2*this._margin});
        this._placeText();
    }

    setColor (color) {
        this._raphRect.attr({fill: color});
    }

    select () {
        this._selected = true;
        this._raphRect.attr({
            'strokeWidth':2,
        });
    };

    unselect () {
        this._selected = false;
        this._raphRect.attr({
            'strokeWidth':0,
        });
    };

    isSelected () {
        return this._selected;
    };

    remove () {
        this._raphRect.remove();
        this._gtext.remove();
    }

    setText (text) {
        this._gtext.attr({text: text});
    }
}

export class Gstaff {

    constructor (top, bottom, raphPaper, index, grid) {
        this._raphPaper = raphPaper;
        this.top = top;
        this.bottom = bottom;
        this.index = index;
        this._glabs = {};
        this._grid = grid;
    }

    createGlabOn (x, text, id) {
        if (this._glabs[id]) {
            throw "GlabAlreadyExists"
        }
        let rectHeight = this.bottom - this.top;
        let newrect = this._raphPaper.rect(x, this.top, 0, rectHeight, 0.2*rectHeight);
        let glabId;
        if (id) {
            glabId = id;
        } else {
            glabId = newrect.id;
        }
        let fontSize = Math.round(rectHeight/5);
        let gtext = this._raphPaper.text(x + Math.round(fontSize/2), this.top + fontSize, text);
        gtext.attr({class: 'glab-text', 'font-size': fontSize + "px"});
        gtext.before(newrect);
        gtext.attr({id: glabId});
        this._glabs[glabId] =  new Glab(glabId, newrect, gtext, this._grid);
        return this._glabs[glabId];
    };

    getGlabById (id) {
        return this._glabs[id];
    };

    removeGlabs () {
        for (let key in this._glabs) {
            this._glabs[key].remove();
        }
        this._glabs = {}
    };
}

export class ScoreImage {

    constructor (svg) {
        this._raphPaper = Snap(svg)
        this._selectedElement;
        this.currentStaffIndex = 0;
        this._ratioWidth;
        this._ratioHeight;
        this._gstaffs = [];
        this._grid;
        this._glabs = {};
    }

    createGglab (x, id, text = "-") {
        let height = this._gstaffs[0].top/2
        let rect  = this._raphPaper.rect(x, 0, 1, height);
        let gglabId;
        if (id) {
            gglabId = id;
        } else {
            gglabId = rect.id;
        }
        rect.attr({id: gglabId});
        let gtext = this._raphPaper.text(x, height*3/4, text);
        gtext.attr({class: "vertical-line-text", 'font-size': height + "px"});
        gtext.before(rect);
        gtext.attr({id: gglabId});
        gtext.attr ({fill : "black"})
        rect.attr({
            opacity: "0.4",
            stroke: "black",
            strokeWidth:1
        });
        let gglab = new Gglab(gglabId, rect, gtext, this._grid)
        this._glabs[gglabId] = gglab;
        return gglab;
    }

    createVerticalLine (x, text, id) {
        let height = this._gstaffs[this._gstaffs.length-1].bottom - this._gstaffs[0].top
        let width = height/20
        let rect  = this._raphPaper.rect(x, this._gstaffs[0].top, width, height, 0.5*width);
        rect.attr({class: "vertical-line-line"});
        let gtext = this._raphPaper.text(x, this._gstaffs[this._gstaffs.length-1].bottom + 20*this._ratioHeight, text);
        let fontSize = 20*this._ratioHeight;
        gtext.attr({class: "vertical-line-text", 'font-size': fontSize + "px"});
        let vlId;
        if (id) {
            vlId = id;
        } else {
            vlId = rect.id;
        }
        rect.attr({id: vlId});
        let vline = new VerticalLine(vlId, rect, gtext, this._grid);
        this._glabs[vlId] = vline;
        return vline;
    }

    createBarNumbers (positions) {
        let fontSize = 18 * this._ratioHeight
        let y = this._gstaffs[0].top + fontSize

        for (let i = 4; i < positions.bars.length; i += 5)
        {
            let gtext = this._raphPaper.text(positions.bars[i] * this._ratioWidth, y, (i+1).toString());
            gtext.attr({'opacity': 0.5, 'text-anchor': 'middle', 'font-size': fontSize + "px"});
        }
    }

    setStaffsPos (positions) {
        for (let i = 0; i < positions.staffs.length; i++) {
            let top = positions.staffs[i].top*this._ratioHeight;
            let bottom = positions.staffs[i].bottom*this._ratioHeight;
            this._gstaffs[i] = new Gstaff(top, bottom, this._raphPaper, i, this._grid);
        }
    };

    setImage (image, width, height, ratioWidth, ratioHeight, grid) {
        this._grid = grid;
        this.clear();
        this._ratioWidth = ratioWidth;
        this._ratioHeight = ratioHeight;
        this._raphPaper.image(
            image, 0,0, width*this._ratioWidth, height*this._ratioHeight
        );
    }

    clear () {
        this._raphPaper.clear();
        this._gstaffs = [];
    }

    getStaff (index) {
        if (this._gstaffs.length == 0) {
            throw "NoStaffDefined"
        }
        if (this._gstaffs.length < index + 1) {
            throw "BadStaffIndex"
        }
        return this._gstaffs[index];
    };

    getStaffAt (y) {
        for (let i = 0; i < this._gstaffs.length; i++) {
            let top = this._gstaffs[i].top;
            let bottom = this._gstaffs[i].bottom;
            if (y > top && y < bottom) {
                this.currentStaffIndex = i;
                return this._gstaffs[i];
            }
        }
        throw "NoStaffHere";
    };

    select (glab) {
        if (glab.isSelected()) {
            return;
        }
        glab.select();
        if (this._selectedElement != null) {
            this._selectedElement.unselect();
        }
        this._selectedElement = glab;
    };

    deleteSelectedElement () {
        var ret = this._selectedElement.id;
        this._selectedElement.remove();
        this._selectedElement = null;
        return ret;
    };

    getGlabById (id) {
        if (this._glabs[id]) {
            return this._glabs[id];
        }
        for (var i = 0; i < this._gstaffs.length; i++) {
            var glab = this._gstaffs[i].getGlabById(id);
            if (glab) {
                return glab;
            }
        }
    };

    clearGlabs () {
        for (var key in this._glabs) {
            this._glabs[key].remove();
        }
        this._glabs = {}

        for (var i = 0; i < this._gstaffs.length; i++) {
            this._gstaffs[i].removeGlabs();
        }
    };

    hasNoVerticalLineHere (x) {
            for (let id in this._glabs) {
                let glab = this._glabs[id];
                if (glab instanceof VerticalLine && x == glab.getX()) {
                    return false;
                }
            }
            return true;
    };

    glabHere (x,y) {
        let snapGlab = Snap.getElementByPoint(x, y);
        return snapGlab != NaN;
    };

    onHeader (y) {
        return y < this._gstaffs[0].top;
    }

    onFooter (y) {
        return y > this._gstaffs[this._gstaffs.length -1].bottom;
    }

    betweenStaffs (y) {
        try {
            this.getStaffAt(y)
        } catch (e) {
            return true;
        }
        return false;
    }
}
