/**
 * class managing a frame put on an svg image
 */
export class Frame {

    /**
     * creates a frame
     * @param {*} paper svg element where the frame has to be put on
     * @param {*} ratio how much the element has been reduced
     */
    constructor(paper,ratio) {
        this.ratio = ratio
        this.frame = paper.rect(0,0,(1/ratio)*100 + "%","100%").attr({
            "fill-opacity":0,
            stroke:"#000",
            strokeWidth:1,
            id:"frame"
        })
    }

    /**
     * moves the frame - triggered by user event
     * @param {*} x new x position for the frame
     */
    move(x) {
        var frameWidth = this.frame.node.width.baseVal.value/2
        if (x -frameWidth< 0) {
            x = frameWidth;
        }
        if (x + frameWidth > window.innerWidth){
            x = window.innerWidth - frameWidth;
        }
        this.frame.attr({x:x-frameWidth});

        return x-frameWidth;
    }

    /**
     * changes the x position of the frame
     * @param {*} x
     */
    setPosition(x) {
        this.frame.attr({
            "x": x
        })
    }

    /**
     * returns the current x position of the frame
     */
    getPosition() {
        return this.frame.attr("x")
    }

    /**
     * returns the ratio
     */
    getRatio() {
        return this.ratio;
    }
}
