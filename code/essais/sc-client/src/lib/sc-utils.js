export function typeToColor (type) {
    let colorOf = {
        'patternA': 'gray',
        'patternB' : 'purple',
        'patternC' : 'magenta',
        'patternD' : 'maroon',
        'S': 'red',
        'CS1': 'blue',
        'CS2': 'green',
        'Structure': 'yellow',
        'Pedal': 'gray',
        'Cad': '#b8a5d5',
    };
    return colorOf[type];
}

var epsilon = 0.01 ;

export function aboutZero(x) {
    return (Math.abs(x - Math.round(x)) < epsilon);
}

export function parseDurationMeasureFrac(str, quartersByMeasure) {
    /**
     * Parse a string representing an offset (in measures) into a float (in quarters)
     * 1/4 should be always a quarter, even in 2/4 or 3/4 measures
     * @param {string} str - string representation such as '14', '14+1/4' or '15-1/8'
     * @param {int} quartersByMeasure - 4 for 4/4
     * @return {float} measure - float offset such as 52.0, 53.0 or 55.5
     */

    var reMeasureFrac = /^(\d+[.]?\d*)(([+-]?\d+)\/(\d+))?$/ ;
    var match = str.match(reMeasureFrac);

    if (!match)
        throw "BadFractionalString";

    var measure = parseFloat(match[1]);

    if (typeof match[3] !== 'undefined')
        measure += parseInt(match[3]) / parseInt(match[4]) * (4 / quartersByMeasure)

    return measure * quartersByMeasure
}


export function toDurationMeasureFrac(offset, quartersByMeasure) {
    /**
     * Convert a float (in quarters) to a string representing an offset (in measures)
     * 1/4 should be always a quarter, even in 2/4 or 3/4 measures
     * @param {float} measure - float offset such as 52.0, 53.0 or 55.5
     * @param {int} quartersByMeasure - 4 for 4/4
     * @return {string} str - string representation such as '14', '14+1/4' or '15-1/8'
     */

    var measure = Math.floor(offset / quartersByMeasure + epsilon)
    var frac = (offset / quartersByMeasure - measure) * (quartersByMeasure / 4)

    if (aboutZero(frac))
        return measure.toString()

    for (var denum of [4,8,16,32, 12,24,48]) {
        var num = frac * denum
        var numRound = Math.floor(num + 0.01)
        if (aboutZero(num - numRound))
            return measure + '+' + numRound + '/' + denum ;
    }
    return (offset / quartersByMeasure).toString()
}


export function parseMeasureFrac(str, quartersByMeasure) {
    return parseDurationMeasureFrac(str, quartersByMeasure) - quartersByMeasure
}

export function toMeasureFrac(offset, quartersByMeasure) {
    return toDurationMeasureFrac(offset + quartersByMeasure, quartersByMeasure)
}
