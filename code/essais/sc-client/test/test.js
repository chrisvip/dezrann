var sc = require('../lib/sc-musical-map')
var scAnalysis = require('../lib/sc-analysis')
var scUtils = require('../lib/sc-utils');
var assert = require('assert');

describe('MusicalMap', function() {

    let normalPos = [
        {
            "x": 10.0,
            "onset": 0.0
        },
        {
            "x": 20.0,
            "onset": 1.0
        },
        {
            "x": 30.0,
            "onset": 2.0
        },
        {
            "x": 40.0,
            "onset": 3.0
        },
        {
            "x": 50.0,
            "onset": 4.0
        },
        {
            "x": 60.0,
            "onset": 5.0
        },
        {
            "x": 70.0,
            "onset": 6.0
        },
        {
            "x": 80.0,
            "onset": 7.0
        },
        {
            "x": 90.0,
            "onset": 8.0
        },
        {
            "x": 100.0,
            "onset": 9.0
        }
    ];
    let map = new sc.MusicalMap();

    describe ('->toMusicalOnset', function () {
        beforeEach(function(){
            map.setPositions(normalPos);
            map.setRatio(1);
        });
        it('should work for an exact position', function() {
            assert.equal(2, map.toMusicalOnset(30.0));
        });
        it('should work for an upper position closer than the next note', function() {
            assert.equal(2, map.toMusicalOnset(34.0));
        });
        it('should work for a lower position closer than the previous note', function() {
            assert.equal(2, map.toMusicalOnset(26.0));
        });
        it('should return the prevous note onset when position is the exact middle between 2 notes', function() {
            assert.equal(2, map.toMusicalOnset(35.0));
        });
        it('should work with first note', function() {
            assert.equal(0, map.toMusicalOnset(10.0));
        });
        it('should work with last note', function() {
            assert.equal(9, map.toMusicalOnset(100.0));
        });
        it('should work before first note', function() {
            assert.equal(0, map.toMusicalOnset(5.0));
        });
        it('should work after last note', function() {
            assert.equal(9, map.toMusicalOnset(105.0));
        });
        it('should work for an exact position (ratio 1/3)', function() {
            map.setRatio(1/3);
            assert.equal(2, map.toMusicalOnset(10.0));
        });
        it('should work for an exact position (ratio 0.5)', function() {
            map.setRatio(0.5);
            assert.equal(2, map.toMusicalOnset(15.0));
        });
        it('should work for an exact position (ratio 5/3)', function() {
            map.setRatio(5/3);
            assert.equal(2, map.toMusicalOnset(50.0));
        });
        it('should work for an exact position (ratio 2)', function() {
            map.setRatio(2);
            assert.equal(2, map.toMusicalOnset(60.0));
        });
    });
    describe ('->toMusicalDuration', function () {
        beforeEach(function(){
            map.setPositions(normalPos);
            map.setRatio(1);
        });
        it('should work with exact notes position : [n1..n2]', function () {
            assert.equal(4, map.toMusicalDuration(40, 40))
        });
        it('should work with exact notes position : [n1..n2] (ratio 1/3)', function () {
            map.setRatio(1/3)
            assert.equal(4, map.toMusicalDuration(13, 13))
        });
        it('should work with exact notes position : [n1..n2] (ratio 0.5)', function () {
            map.setRatio(0.5)
            assert.equal(4, map.toMusicalDuration(20, 20))
        });
        it('should work with exact notes position : [n1..n2] (ratio 5/3)', function () {
            map.setRatio(5/3)
            assert.equal(4, map.toMusicalDuration(66, 66))
        });
        it('should work with exact notes position : [n1..n2] (ratio 2)', function () {
            map.setRatio(2)
            assert.equal(4, map.toMusicalDuration(80, 80))
        });
        it('should work with approx notes positions : [..n1..]..n2', function () {
            assert.equal(4, map.toMusicalDuration(36, 40))
        });
        it('should work with approx notes positions : [..n1..n2..]', function () {
            assert.equal(4, map.toMusicalDuration(36, 45))
        });
        it('should work with approx notes positions : n1..[..n2..]', function () {
            assert.equal(4, map.toMusicalDuration(44, 40))
        });
        it('should work with approx notes positions : n1..[..]..n2', function () {
            assert.equal(4, map.toMusicalDuration(44, 32))
        });
        it('should return zero for length of zero', function () {
            assert.equal(0, map.toMusicalDuration(40, 0))
        });
        it('should return the right length for an out of bound graphical length', function () {
            assert.equal(6, map.toMusicalDuration(40, 300))
        });
        it('should return the piece length for an out of bound x', function () {
            assert.equal(4, map.toMusicalDuration(0, 50))
        });
        it('should return the piece length for an out of bound x and graphical length', function () {
            assert.equal(9, map.toMusicalDuration(0, 300))
        });
    });
    describe ('->toGraphicalX', function () {
        beforeEach(function(){
            map.setPositions(normalPos);
            map.setRatio(1);
        });
        it('should work for an existing onset', function() {
            assert.equal(30, map.toGraphicalX(2));
        });
        it('should work for an upper onset closer than the next note', function() {
            assert.equal(30, map.toGraphicalX(2.25));
        });
        it('should work for a lower onset closer than the previous note', function() {
            assert.equal(30, map.toGraphicalX(1.75));
        });
        it('should return the previous note position when onset is the exact middle between 2 notes', function() {
            assert.equal(30, map.toGraphicalX(2.5));
        });
        it('should work with first note', function() {
            assert.equal(10, map.toGraphicalX(0));
        });
        it('should work with last note', function() {
            assert.equal(100, map.toGraphicalX(9));
        });
        it('should work after last note', function() {
            assert.equal(100, map.toGraphicalX(10));
        });
        it('should work for an existing onset (ratio 1/3)', function() {
            map.setRatio(1/3);
            assert.equal(10, map.toGraphicalX(2));
        });
        it('should work for an existing onset (ratio 0.5)', function() {
            map.setRatio(0.5);
            assert.equal(15, map.toGraphicalX(2));
        });
        it('should work for an existing onset (ratio 5/3)', function() {
            map.setRatio(5/3);
            assert.equal(50, map.toGraphicalX(2));
        });
        it('should work for an existing onset (ratio 2)', function() {
            map.setRatio(2);
            assert.equal(60, map.toGraphicalX(2));
        });
    });
    describe ('->toGraphicalLength', function () {
        beforeEach(function(){
            map.setPositions(normalPos);
            map.setRatio(1);
        });
        it('should work with exact notes onset : [n1..n2]', function () {
            assert.equal(40, map.toGraphicalLength(3, 4))
        });
        it('should work with exact notes onset : [n1..n2] (ratio 1/3)', function () {
            map.setRatio(1/3)
            assert.equal(14, map.toGraphicalLength(3, 4))
        });
        it('should work with exact notes onset : [n1..n2] (ratio 0.5)', function () {
            map.setRatio(0.5)
            assert.equal(20, map.toGraphicalLength(3, 4))
        });
        it('should work with exact notes onset : [n1..n2] (ratio 5/3)', function () {
            map.setRatio(5/3)
            assert.equal(66, map.toGraphicalLength(3, 4))
        });
        it('should work with exact notes onset : [n1..n2] (ratio 2)', function () {
            map.setRatio(2)
            assert.equal(80, map.toGraphicalLength(3, 4))
        });
        it('should work with non existing onset : [..n1..]..n2', function () {
            assert.equal(40, map.toGraphicalLength(2.75, 4))
        });
        it('should work with approx non existing onset : [..n1..n2..]', function () {
            assert.equal(40, map.toGraphicalLength(2.75, 4.5))
        });
        it('should work with approx non existing onset : n1..[..n2..]', function () {
            assert.equal(40, map.toGraphicalLength(3.25, 4))
        });
        it('should work with approx non existing onset : n1..[..]..n2', function () {
            assert.equal(40, map.toGraphicalLength(3.25, 3.5))
        });
        it('should return zero for duration of zero', function () {
            assert.equal(0, map.toGraphicalLength(3, 0))
        });
        it('should return the right length for an out of bound duration', function () {
            assert.equal(60, map.toGraphicalLength(3, 30))
        });
        it('should return the right length for an out of bound start onset', function () {
            assert.equal(50, map.toGraphicalLength(0, 5))
        });
        it('should return the piece length for an out of bound start onset and duration', function () {
            assert.equal(90, map.toGraphicalLength(0, 30))
        });
    });
});

describe('MeasureMusicalMap', function() {

    let narrowPos = [10.0, 20.0, 30.0, 40.0];
    let normalPos = [100.0, 200.0, 300.0, 400.0];
    let variablePos = [100.0, 190.0, 313.0, 512];
    let floatPos = [100.0, 189.66, 300.134, 456.789];
    let map = new sc.MeasureMusicalMap();

    describe ('->toGraphicalLength', function () {
        beforeEach(function(){
            map.setBarsPositions(normalPos);
            map.setRatio(1);
        });
        it('should return bar to bar length', function () {
            assert.equal(100, map.toGraphicalLength(4, 4));
        });
        it('should return bar to bar length for variable positions', function () {
            map.setBarsPositions(variablePos);
            assert.equal(123, map.toGraphicalLength(4, 4));
        });
        it('should return bar to bar length for float positions', function () {
            map.setBarsPositions(floatPos);
            assert.equal(110.47400000000002, map.toGraphicalLength(4, 4));
        });
        it('should not exceed last bar', function () {
            assert.equal(-1, map.toGraphicalLength(10.0, 4.0));
        });
    });

    describe ('->toMusicalDuration', function () {
        beforeEach(function(){
            map.setBarsPositions(normalPos);
            map.setRatio(1);
        });
        it('should return -1 if startx is before first bar', function () {
            assert.equal(-1, map.toMusicalDuration(50.0));
        });
        it('should return -1 if startx is after last bar', function () {
            assert.equal(-1, map.toMusicalDuration(450.0));
        });
        it('should return 4 from bar to bar', function () {
            assert.equal(4, map.toMusicalDuration(100.0, 100.0));
        });
        it('should return 4 from mid bar to mid bar', function () {
            assert.equal(4, map.toMusicalDuration(250.0, 100.0));
        });
        it('should return 2.5 inside measure', function () {
            assert.equal(2.5, map.toMusicalDuration(225.0, 62.5));
        });
        it('should return 2.5 across bar', function () {
            assert.equal(2.5, map.toMusicalDuration(275.0, 62.5));
        });
        it('should not exceed last bar', function () {
            assert.equal(-1, map.toMusicalDuration(350.0, 100.0));
        });
    });

    describe ('->toMusicalOffset', function () {
        beforeEach(function(){
            map.setBarsPositions(normalPos);
            map.setRatio(1);
        });
        it('should return an offset of 0 for the position of the first bar', function() {
            assert.equal(0, map.toMusicalOffset(100.0));
        });
        it('should return the last offset for the last bar', function(){
            assert.equal(16, map.toMusicalOffset(400.0));
        });
        it('should return half measure offsets for the middle position', function(){
            assert.equal(6.0, map.toMusicalOffset(250.0));
        });
        it('should return half measure offsets for the large rounded middle positionss', function(){
            assert.equal(6.0, map.toMusicalOffset(253.0));
            assert.equal(6.0, map.toMusicalOffset(247.0));
        });
        it('should return half measure offsets for the middle position for variable pos', function(){
            map.setBarsPositions(variablePos);
            assert.equal(6.0, map.toMusicalOffset(252.5));
        });
        it('should return half measure offsets for the middle position for float pos', function(){
            map.setBarsPositions(floatPos);
            assert.equal(6.0, map.toMusicalOffset(244.897));
        });
        it('should return -1 for offset before first bar', function(){
            assert.equal(-1, map.toMusicalOffset(50.0));
        });
        it('should return -1 for offset beyond last bar', function(){
            assert.equal(-1,map.toMusicalOffset(450.0));
        });
        it('should return correct offset when x is on bar', function() {
            assert.equal(8, map.toMusicalOffset(300.0));
        });
        it('should return correct offset when x is on bar at a ration of 0.3333', function() {
            map.setRatio(0.3333);
            assert.equal(8, map.toMusicalOffset(100.0));
        });
        it('should return correct offset when x is on bar at a ration of 0.5', function() {
            map.setRatio(0.5);
            assert.equal(8, map.toMusicalOffset(150.0));
        });
        it('should return correct offset when x is on bar at a ration of 2', function() {
            map.setRatio(2);
            assert.equal(8, map.toMusicalOffset(600.0));
        });
    });

    describe ('->toGraphicalX', function () {
        describe ('normal positions', function (){
            beforeEach(function(){
                map.setBarsPositions(normalPos);
                map.setRatio(1);
            });
            it('should return -1 if offset is after last bar', function () {
                assert.equal(-1, map.toGraphicalX(14));
            });
            it('should return the last bar position for last offset', function () {
                assert.equal(400, map.toGraphicalX(12));
            });
            it('should return 75.0 for an offset of 2.0 at a ratio of 0.5', function () {
                map.setRatio(0.5);
                assert.equal(75.0, map.toGraphicalX(2.0));
            });
            it('should return 49.995 for an offset of 2.0 at a ratio of 0.3333', function () {
                map.setRatio(0.3333);
                assert.equal(49.995, map.toGraphicalX(2.0));
            });
            it('should return 300.0 for an offset of 2.0 at a ratio of 2', function () {
                map.setRatio(2);
                assert.equal(300.0, map.toGraphicalX(2.0));
            });
        });
        describe ('Specific positions', function (){
            beforeEach(function(){
                map.setRatio(1);
            });
            it('should return 15.0 for an offset of 2.0 for narrow positions', function () {
                map.setBarsPositions(narrowPos);
                assert.equal(15.0, map.toGraphicalX(2.0));
            });
            it('should return 145.0 for an offset of 2.0 for variable positions', function () {
                map.setBarsPositions(variablePos);
                assert.equal(145.0, map.toGraphicalX(2.0));
            });
            it('should return 144.82999999999998 for an offset of 2.0 for float positions', function () {
                map.setBarsPositions(floatPos);
                assert.equal(144.82999999999998, map.toGraphicalX(2.0));
            });
        });
    });
});

describe('Schema', function() {
    let schema = new scAnalysis.Schema();
    let labels = {"labels":[{"label1":{"start": 8.5, "duration": 8.0, "type": "S"}}]};
    let label = new scAnalysis.Label(10,16,'CS1',"tag",1);
    let labelToJson =   '{\n'+
                        '    "labels": [\n'+
                        '        {\n'+
                        '            "start": 10,\n'+
                        '            "duration": 16,\n'+
                        '            "type": "CS1",\n'+
                        '            "tag": "tag",\n'+
                        '            "staff": 1\n'+
                        '        }\n'+
                        '    ]\n'+
                        '}'
    let labelToString = "1(10[16],CS1,tag)\ncadences -> \n"

    describe ('->getLabel', function () {
        beforeEach(function(){
            schema.addLabel("label1", label);
        })
        it('should return label', function () {
            assert.equal(label, schema.getLabel("label1"));
        })
    });
    describe ('->toString', function () {
        beforeEach(function(){
            schema = new scAnalysis.Schema();
            schema.addLabel("label1", label);
        })
        it('should return json', function() {
            assert.equal(schema.toJson(),labelToJson);
        });
        it('should return string', function() {
            assert.equal(schema.toString(),labelToString);
        });
    })
    describe ('->deleteLabel', function () {
        it('should remain no labels', function() {
            schema.addLabel("label1", label);
            schema.deleteLabel("label1");
            assert.equal(null,schema.getLabel("label1"))
        });
    })
    describe ('->getLabelsByType', function () {
        it('should return one label of type CS1', function() {
            schema.addLabel("label1", label);
            assert.equal(1,schema.getLabelsByType("CS1").length)
            assert.equal(label,schema.getLabelsByType("CS1")[0])
        });
    })
    describe ('->getLabelsByStaff', function () {
        it('should return one label in staff 1', function() {
            schema.addLabel("label1", label);
            assert.equal(1,schema.getLabelsByStaff(1).length)
            assert.equal(label,schema.getLabelsByStaff(1)[0])
        });
    })

});


describe('sc-utils.js: string/float offset conversions', function() {

    it('toDurationMeasureFrac()', function () {
        assert.equal(scUtils.toDurationMeasureFrac(56.0, 4), '14');
        assert.equal(scUtils.toDurationMeasureFrac(57.0, 4), '14+1/4');
        assert.equal(scUtils.toDurationMeasureFrac(58.667, 4), '14+8/12');
        assert.equal(scUtils.toDurationMeasureFrac(56.492, 4), '14.123');
        assert.equal(scUtils.toDurationMeasureFrac(44.0, 3), '14+2/4');
    });

    it('parseDurationMeasureFrac()', function () {
        assert.equal(scUtils.parseDurationMeasureFrac('14+1/4', 4), 57.0);
        assert.equal(scUtils.parseDurationMeasureFrac('14+1/4', 3), 43.0);
        assert.equal(scUtils.parseDurationMeasureFrac('14-1/4', 4), 55.0);
        assert.equal(scUtils.parseDurationMeasureFrac('14', 4), 56.0);
        assert.equal(scUtils.parseDurationMeasureFrac('14.4', 4), 57.6);
    });

    it('(to/parse)MeasureFrac()', function () {
        assert.equal(scUtils.toMeasureFrac(57.0, 4), '15+1/4');
        assert.equal(scUtils.parseMeasureFrac('15+1/4', 4), 57.0);
    });
});
