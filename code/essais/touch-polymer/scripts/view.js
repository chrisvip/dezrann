class View {

    constructor(width,height,src,id,ratio) {
        this.width = width/ratio;
        this.height = height/ratio;
        this.ratio = ratio;
        this.paper = Snap("#full").attr({
            height:this.height,
            width:this.width
        })

        this.group = this.paper.g()

        this.group.image(src,0,0,"100%","100%").attr({
            id:id
        })


        this.frame = this.paper.rect(5,0,(1/this.ratio)*100 + "%","100%").attr({
            "fill-opacity":0,
            stroke:"#000",
            strokeWidth:1,
            id:"frame"
        })
    }

    moveFrame(x) {
        var frameWidth = this.frame.node.width.baseVal.value/2
        if (x < 0) {
            x = frameWidth;
        }
        if (x + frameWidth > window.innerWidth){
            x = window.innerWidth - frameWidth;
        }
        this.frame.attr({x:x-frameWidth});

        return x-frameWidth;
    }

    scroll(x,frame) {
        if (frame) {
            var offset = Snap("#main").attr("transform").globalMatrix.e;
            this.frame.attr({x:-offset/this.ratio});
        }
    }

    createMiniRect(x,y,id,offset) {
        var mini_y = y/this.ratio - 40/this.ratio;
        this.group.rect((x-offset)/this.ratio,mini_y,0,80/this.ratio,2).attr({
            fill:"red",
            "fill-opacity":0.3,
            class:"rect",
            id:"mini-" +id
        })
    }

    resizeMiniRect(id,x,width) {
        Snap('#mini-' +id).attr({
            x:parseInt(x/this.ratio),
            width:width/this.ratio
        })
    }
}