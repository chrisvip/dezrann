class Paper {
    
    constructor(width,height) {
        this.width = width;
        this.height = height;
        this.ratio = width/window.innerWidth;
        this.paper = Snap('#zoom').attr({
                        height: height,
                        width: width,
                    });
        
        this.group = this.paper.g().attr({transform:'matrix(1,0,0,1,0,0)',id:"main"});
        this.offset = 0;
    }

    resize(width,height) {
        this.width = width;
        this.height = height;
        this.paper.attr({width:width,height:height});
        Snap('#detail').attr({width:width,height:height});
    }


    attachImg(src,id) {
       var img = this.paper.image(src,0,0,this.width,this.height).attr({
           id:id,
           //preserveAspectRatio:"xMinYMin",
       });
       this.group.add(img);
    }

    scrollImg(x) {
        this.offset += x 
        if (this.offset > 0) {
            this.offset = 0;
        }

        if (-(this.offset) > this.width - window.innerWidth) {
            this.offset = -(this.width - window.innerWidth);
        }
        this.group.attr({transform:"matrix(1,0,0,1,"+((this.offset)+",0)")});
    }

    moveImg(x,width) {
        var y = x - width/2 
        this.group.attr({transform:"matrix(1,0,0,1,"+(-(y*this.ratio)+",0)")});
    }

    createGlab(x,y) {
        this.stopSelection();
        this.offset = this.group.attr("transform").globalMatrix.e;
        this.currentRect = this.group.rect(x - this.offset,y - 30,0,40,10).attr({
            fill:"red",
            "fill-opacity":0.3,
            class:"rect",
            })
            this.currentRect.attr({id:this.currentRect.id})
    }

    drawGlab(dx,dy,x) {
        //TODO : adapter angle à écran => ? 
        if (this.calcAngle(dx,dy) > 0.1 && this.calcAngle(dx,dy) < 2.7) {
            return;
        }
        
        x -= this.offset; 

        //stabilo de droite à gauche
        if (dx < 0) {
            dx = - dx;
            x -= dx;
        }
        
        this.resizeRect(dx);
        this.moveX(x);
    }

    removeGlab() {
        this.currentRect.remove();
    }

    selectCurrent() {
        this.changeOpacity(0.6);
        this.displayArrows();
    }

    stopSelection() {
        try {
            this.changeOpacity(0.3);
            this.paper.selectAll('.arrow').forEach(function(e){
                        e.remove();
                    },this);

            }
        catch (e) {
        }
    }    

    calcAngle(xLength,yLength) {
        var hyp = Math.sqrt(Math.pow(xLength,2) + Math.pow(yLength,2));
        return Math.acos(xLength/hyp);
    }

    displayArrows(){
        //TODO : adapter la taille des flèches 
        var x = parseInt(this.currentRect.attr("x")) + 10;
        var mid = parseInt(this.currentRect.attr("y")) + this.currentRect.attr("height")/2;
        var left = this.group.polyline(x,mid,x+15,mid-15,x+15,mid+15).attr({
                fill:"#FFF",
                class:'arrow',
                id:"left"
        })
        x = parseInt(this.currentRect.attr("x")) + parseInt(this.currentRect.attr("width")) -10;
        var right = this.group.polyline(x,mid,x-15,mid-15,x-15,mid+15).attr({
                fill:"#FFF",
                class:'arrow',
                id:"right"
        })
    }

    //Modification des rectangles

    resizeRect(width) {
        this.currentRect.attr({
            width:width,
        })
    }

    changeOpacity(opacity) {
        this.currentRect.attr({
            "fill-opacity":opacity
        })
    }

    moveX(x) {
        this.currentRect.attr({
            x:x
        })
    }

    //Gestion des évènements

    onRectClick(id) {
        event.preventDefault();
        this.stopSelection();
        this.currentRect = Snap('#' +id);
        this.changeOpacity(0.6);
        this.displayArrows();
    }

    startResizeRect(x,y) {
        this.arrowX = x;
        this.arrowY = y;
        this.currentWidth = this.currentRect.attr("width");
        this.currentX = this.currentRect.attr("x");
    }

    moveLeft(x,y) {
        var xLength = parseInt(x) - parseInt(this.arrowX);
        var yLength = parseInt(y) - parseInt(this.arrowY);
        if (this.calcAngle(xLength,yLength) < 0.09 || this.calcAngle(xLength,yLength) > 2.2 ) {
            var width = parseInt(this.currentWidth - (x - this.arrowX));
            this.resizeRect(width);
            this.moveX(parseInt(this.currentX - (this.arrowX - x)));
            var points = Snap('#left').attr("points");
            var newX = parseInt(this.currentX - (this.arrowX - x)) + 15;
            var off = parseInt(newX) +15
            Snap('#left').attr({
                points: newX+","+points[1]+","+off+","+points[3]+","+off+","+points[5]
            })
        }
    }

    moveRight(x,y) {
        if (this.calcAngle(x - this.arrowX,y-this.arrowY) < 0.09 || this.calcAngle(x-this.arrowX,y-this.arrowY)> 2.2) {
            var width = parseInt(this.currentWidth-this.arrowX + x)
            this.resizeRect(width);
            x = parseInt(this.currentX) + parseInt(this.currentRect.attr("width")) - 10
            var points = Snap('#right').attr("points");
            var off = parseInt(x) -15
            Snap('#right').attr({
                points: x+","+points[1]+","+off+","+points[3]+","+off+","+points[5]
            })
        }
    }
}