#Tests des évènements tactiles et actions possibles avec Snap.svg 

### Réalisations : 
* Vue Globale / Vue zoomée avec éléments Snap
* Création de rectangles colorés par un long click (500 ms pour la rapidité des tests)
* Gestion des rectangles par rapport au scroll
* Report des rectangles créés sur la vue globale 
* Récupération de la taille de l'image dynamiquement
* Création des rectangles par un geste "Stabilo"
* Modification de la taille des rectangles possible

### TODO :
* Gestion multi-navigateurs( uniquement testé avec Chrome)
* Adaptation taille écran (convient pour tablette mais pas mobile)