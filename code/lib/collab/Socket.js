const io = require('socket.io-client');
const PendingOperations = require('./PendingOperations')

const url = "http://localhost:3000"
const options ={
  transports: ['websocket'],
  'force new connection': true
};

class Socket {

  constructor () {
    this._pendingOperations = new PendingOperations()
    this._name = "socket"
    this._emitted = 0
    this._accepted = 0
    this._denied = 0
  }

  get name () {
    return this._name
  }

  set name (value) {
    this._name = value
  }

  _acceptCallBack (data) {
    this._accepted++
    if (this._lastKnownVersion < data.current_operation.version) {
      this._lastKnownVersion = data.current_operation.version
    }
    this._pendingOperations.accept(
      data.current_operation.op.id,
      data.current_operation.version
    )
    this._applyPendingOperations()
  }

  _denyCallBack (data) {
    this._denied++
    this._pendingOperations.delete(data.last_op.id)
    this._sock.emit('analysis-demande', {});
  }

  _updateAnalysisCallBack (data) {
    if (data.piece != this._analysis.pieceName) return
    if (this._displayedVersion == 0 || this._displayedVersion != data.current_version) {
      this._analysis.apply(data.current_analysis, "collab")
      this._pendingOperations.purge()
      this._displayedVersion = data.current_version
      this._lastKnownVersion = data.current_version
    }
  }

  /*
  * Apply only operation from others before a version break (see purgeFrom doc)
  * Is there a case where there is a pending opration not accepted?
  */
  _applyPendingOperations () {
    if (this._pendingOperations.allAccepted && this._pendingOperations.length > 0) {
      this._pendingOperations
      .purgeFrom(this._pendingOperations.oldestVersion)
      .forEach(o => {
        if (!o.applied) {
          this._analysis.applyOperation(o.op);
        }
        this._displayedVersion = o.version
      })
    }
  }
  /*
   * ON CHANGE-ANALYSIS (from others)
   * - Ask for current analysis if the number of pending operations from
   * others is too high (== RESET) and then return
   * OR
   * - Store the operation
   * - Apply and remove operations that can be applied only if all local
   * pending operations are accepted or denied. Operation can be applied
   * only if the oldest pending operation from the server has the version
   * this._displayed + 1.
   */
  _changeAnalysisCallBack (data) {
    if (this._lastKnownVersion < data.current_operation.version) {
      this._lastKnownVersion = data.current_operation.version
    }
    if (this._pendingOperations.length >= 100) {
      this._sock.emit('analysis-demande', {});
    } else {
      this._pendingOperations.add(data.current_operation.op, data.current_operation.version)
      this._applyPendingOperations()
    }
  }

  async connect (alert = () => {}) {
    this._displayedVersion = 0
    this._sock = await this._connect()
    this._sock.on('disconnect', () => { alert() });
    this._sock.on('accept_channel', data => {
      this._acceptCallBack(data)
    })
    this._sock.on('deny_channel', data => {
      this._denyCallBack(data)
    })
    this._sock.on('analysis-channel', data => {
      this._updateAnalysisCallBack(data)
    });
    this._sock.on('change-analysis', data => {
      this._changeAnalysisCallBack (data)
    });
  }

  get analysis  () {
    return this._analysis
  }

  set analysis (value) {
    this._analysis = value
  }

  async _connect () {
    let sock = io.connect(url, options)
    return new Promise ((resolve) => {
      sock.on('connect', () => {
        resolve(sock)
      })
    })
  }

  askForCurrentAnalysis () {
    this._displayedVersion = 0
    this._sock.emit('analysis-demande', {
      piece : this._analysis.pieceName
    })
  }

  emitOperation(operation) {
    this._pendingOperations.addLocal(operation, this._lastKnownVersion)
    let emitMessage = operation.action.toLowerCase() + '-label'
    if (operation.action == "LOAD_NEW_ANALYSIS") {
      emitMessage = "load-new-analysis"
    }
    this._sock.emit(emitMessage, {
      server_version : this._lastKnownVersion,
      op : operation
    });
    this._emitted++
  }

  disconnect () {
    this._sock.disconnect()
  }

  reinit () {
    this._sock.emit('reinit')
  }

}

module.exports = Socket
