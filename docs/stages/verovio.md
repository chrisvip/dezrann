
# Visualisation dynamique et responsive de partitions de musique

L'équipe Algomus développe Dezrann, une application web pour lire et annoter des partitions musicales. 
Cette application utilise le framework Polymer qui implémente la technologie des *Web Components*
(futur standard du W3C). 
Nous réalisons un ensemble de balises HTML paramétrables qui s'intègrent aisément 
dans une page web à la manière des balises HTML5 vidéo ou audio.
Dezrann est utilisé d'un côté par des classes de collèges pour découvrir la musique,
et de l'autre, par des musicologues annotant des corpus.

Pour l'instant, notre rendu de partition est statique et limité.
L'objectif du projet est d'évaluer Verovio (www.verovio.org), une librairie javascript moderne
pour afficher de la musique sous forme de MEI et interagir avec elle.
Concrètement, le projet commencera par utiliser Verovio sur des exemples simples, puis
tentera de construire un composant web prototype qui proposerait différentes options
de récupération des positions voire d'interaction avec des notes ou d'autres objets de la partition. 
En cas de succès, dans un second temps une intégration à Dezrann pourra être envisagée
ainsi qu'un test avec nos collègues musiciens.

Mots clés: partitions musicales et analyse, synchronisation, animation, Polymer, Web components, javascript.

Liens :
 https://www.verovio.org
 http://www.dezrann.net
 http://polymer-project.org
 