# Demarrage du PJI: vendredi 13 janvier 2017

## Affichage

- gestion du viewport, taille du device
- ajout de contraintes:
    - plein écran
    - pas de scrolling vertical
- éléments à afficher:
    - seulement le score?
    - aparaition de panneau lateraux?
    - fenêtres modales
- Format PAYSAGE

## Gestuelle mobile

- sur l'existant:
    - déplacement global du svg
    - déplacement d'un rectangle
    - création d'élement -> geste du stabilo
- élément à venir
    - édition d'un label
    - d'autres éléments: labels globaux, triangles, barres verticales
    - contrôles: chargement/ sauvegarde d'analyse
- voir ce qui existe: applis se rapprochant
- Material design: google

## technos

- snap.js (raphael)
- Polymer
- faire les tutos

mercredi 14h
jeudi 17h20.
