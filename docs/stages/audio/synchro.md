# Synchronisation musicale semi-automatique partition/audio


L'équipe Algomus développe Dezrann, une application web open-source pour lire et annoter des partitions musicales (www.dezrann.net). Cette application utilise
le framework Polymer qui implémente la technologie des *Web Components*
(futur standard du W3C). Nous réalisons un ensemble de balises
HTML paramétrables qui s'intègrent aisément dans une page web à la manière des
balises HTML5 vidéo ou audio. Dezrann est déjà capable de jouer un fichier audio à la condition d'avoir la synchronisation entre audio et parttion.

L'objectif du projet est d'avoir une procédure de synchronisation semi-automatique entre audio et partitions (fichiers MIDI). Cela permettra d'avoir des centaines ou milliers de fichiers audio disponibles via Dezrann à partir d'enregsitrements libre, et, à terme, de pouvoir lancer une partition de Dezrann en donnant un mp3.

Concrètement, le travail comportera trois points:

- Lire de la bibliographie sur l'alignement partition / audio

- Implémenter un de ces algorithmes et le tester (python, avec une librairie audio type librosa)

- Implémenter un mode d'édition de synchronisation pour raffiner une synchronisation pré-calculée ou existante (développement web client.js / Polymer)


Le code réalisé sera testé, et, en cas de succès, sera intégré à l'application pour être utilisé en situation réelle, dont dans un partenariat en classes de collège avec l'académie d'Amiens.

Mots clés : partition musicales, Polymer, synchronisation musicale, javascript, python

Liens associés :

    Une méthode d'alignement audio/symbolique : https://www.cs.cmu.edu/~rbd/papers/waspaa03alignment.pdf
    La librairie python librosa : https://librosa.github.io/librosa/
    Article sur Dezrann : https://hal.archives-ouvertes.fr/hal-01796787


