# Un réseau social pour annoter la musique

L'équipe Algomus développe l'application web 'Dezrann' pour lire et annoter
des partitions musicales (prototype sur http://dezrann.net/). L'annotation se fait en ajoutant
des éléments graphiques sur la partition: les 'labels'. Cette application utilise
le framework Polymer qui implémente la technologie des *Web Components*
(standard du W3C). Nous réalisons un ensemble de balises
HTML paramétrables qui s'intègrent aisément dans une page web à la manière des
balises HTML5 vidéo ou audio.

L'objectif du projet est de pouvoir annoter à plusieurs et en temps réel une
partition de musique. Ainsi les manipulations de labels (création, déplacements,
agrandissements...) effectuées par un utilisateur seront visibles instantanément
par les autres. Un chat sera également disponible et pourra constituer une
première version de l'application à livrer.

Techniquement, le travail consistera à:
1- étudier les websockets à travers la bibliothèque javascript
`socket.io`,
2- utiliser `socket.io` dans le cadre des *Web Components* (Polymer),
3- intégrer ces nouvelles fonctionalités à l'application 'Dezrann'.

Mots clés : partitions musicales, Polymer, websocket, socket.io, javascript.

Liens :
 - http://dezrann.net/
 - https://socket.io/
 - https://www.polymer-project.org/
