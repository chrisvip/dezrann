# ScoreCloud - ARchi client / serveur

## Editeur 'standalone'

### Sur le serveur

* stockage
  * analyses
  * partition: images, reperage, autres fichiers (krn, midi, musicXML, MEI...)
  * son: mp3, midi, fichier de synchro
  * etat de la session (auto save)
  * catégories de labels
  * gestion des versions?
* traitement
  * fabrication ressource de partitions
    * image + reperage: lilypond, autres...
    * codage vexflow

### Sur le client

Toutes les manipulations graphiques sont faites dans le browser.
Chaque manip est enregistrée sur le serveur en tache de fond.
Besoin du réseau uniquement pour charger une nouvelle analyse ou une nouvelle partition à analyser.
C'est à dire charger depuis un catalogue de partition/analyse ou de fichier locaux à uploader.

* Il faut prévoir un cache en cas d'abscence du réseau. Ce cache permettra:
  * de reprendre une analyse hors ligne
  * de commencer une analyse hors ligne? -> cela veut dire que les traitement côté serveur doivent-être côté client...

## Editeur collaboratif

### Sur le serveur

* stockage
  * profils utilisateur
  * notation des analyses (définir des métriques sociales: les plus vues, utilisées, modifiées, forkées...)
* collaboration temps réel (web socket, bdd temps réel (type firebase))

### Sur le client

## Technos

### Client

* Polymer: web component
* Fabric.js: bib de manipulation graphique
* son...

### Serveur

* node.js: même langage que le client (JS) => possibilité de passer des modules serveurs côté client (cache, offline)
* python (django?): compatibilité music21, framework algomus, ly2video
* BDD: temps réel?