# ScoreCloud - Journée au vert le 4 juillet 2016

## Démo

* visualisation d'une vidéo:
  * besoin de caler la partition à une mesure, un onset ou à un label précis.
  * besoin d'utiliser le svg comme minimap.
  * analogie avec l'utilisation de la minimap dans autom
* démo du prototype
  * labels de portée
    * catalogue de types, 3 types par défaut
    * déplacement d'une portée à l'autre?
  * les labels globaux: cadences, degres, structure, rectangles globaux
* Utilisation des tablettes
  * pas de minimap sur les petits écrans
  * Création d'un label de portée par exemple
* presentation du composant
* référence de position = L'onset
  * chaque vue possède un repérage graphique différent
    * précision/définition differente
    * regulière ou non
  * diagramme de classes
    * concept de map: retrouver qqchose à partir d'une coordonnée graphique
    * ce qui est utile dans la note c'est onset et sa durée (ou onset de la note suivante)
    * etat du dessin
    * etat des calques d'analyse
    * ScoreMap abstrait. Peut être du vexflow.
  * pb d'alignement: nécessité d'avoir un quadrillage par portée
* les fichiers d'entrée
  * données brutes: image + positions
  * fichier lilypond: contrainte de formatage fortes?
  * fichier musicaux: krn, musicXML, MEI, midi. gen lilypond puis image + positions
  * fichier d'analyse: truth, MEI, autres (JSON) ...
* multi-analyse
* cloud:
  * gestion de profil
  * partage
  * communications (messagerie)
  * collaboration instantanée

## TODO list

* Finaliser diagrammes de classes -> 2 jours (septembre)
* Fabriquer les données brutes de la fugue 2 (image + positions) -> 2 jours (mi septembre)
* Premiere version du composant d'édition avec labels de portée -> 5 jours (mi octobre)
* Gestion d'un catalogue de type de label (S, CS, pedale, marche harmonique, début de sujets...)
  Subdivisé en type d'analyse (fugue, sonate, texture...). -> 2 jours (novembre)
* Gestion de la persistence de l'analyse (sauver/ouvrir) -> 3 jours (fin décembre)
* Edition des labels globaux -> mi décembre
  * Edition des cadences -> 1 jour
  * Edition des triangles -> 1 jour
  * Edition des rectangle globaux -> 1 jour
* Gestion de la navigation dans la zone d'édition -> 2 jours (janvier)
* Composant minimap en interaction avec le composant d'édition -> 2 jours (fin janvier)
* Gestion de la persistence de la navigation -> 2 jours (mi février)
* Charger les données brutes d'une partition (image + positions) -> 1 jour
* Constituer un corpus d'exemples utilisables (fugues, sonates...) -> mars 2017
* Charger un fichier krn
* Charger un fichier musicXML
* Charger un fichier MEI
* Charger un fichier midi
* Charger un fichier lilypond
* Editeur avec gestion des calques
* Version multi-analyse de l'éditeur
* Version multi-utilisateur: authentification et gestion de profil
* Version multi-utilisateur: fonctionnalités de partage
* Version multi-utilisateur: collaboration temps réel
* Ajout d'outils d'analyse automatiques...
